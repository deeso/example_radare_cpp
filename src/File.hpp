
/*
    
   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifndef FILE_DEF_H
#define FILE_DEF_H

#include <Poco/StringTokenizer.h>
#include <Poco/NumberParser.h>

#include <r_core.h>
#include <r_bin.h>
#include <r_pe.h>
#include <r_elf.h>

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/backend/Postgres>
#include <Wt/WString>

#include <boost/filesystem.hpp>
#include <boost/container/set.hpp>
#include <boost/container/map.hpp>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>




#include "util.hpp"
//#include "Section.hpp"


namespace dbo = Wt::Dbo;

//class Section;

class File{

    enum HASH_TYPE { IMPORT, SYMBOL, SECTION, STRING, META};


    boost::container::set<long> StringAssociations;
    boost::container::set<long> SectionAssociations;
    boost::container::set<long> MetaAssociations;
    boost::container::set<long> SymbolAssociations;
    boost::container::set<long> ImportAssociations;


    //boost::container::map< long, Section *> Sections;



    long HashId;
    
    std::string Sha256;
    std::string Md5;
    std::string FuzzyHash;

    
    std::string Architecture;
    int Bits;

    std::string RClass;
    std::string BClass;
    std::string Machine;

    std::string OS;
    std::string FileType;
    std::string Subsystem;
    std::string RPath;
    

    bool IsBigEndian;
    bool HasVirtualAddress;
    long EntryPoint;
    long TimeDateStamp;
    double Entropy;
    double FractionalEntropy;

    
    std::string Path;
    std::string FileName;
    std::string Extension;
    
    long ImageBaseOffset;
    long SizeOfImage;
    long FileSize;

    bool hasAssociation(  HASH_TYPE x, long hash){
        boost::container::set<long> *the_hash_set = NULL;
        if (x == SYMBOL)
            the_hash_set = & (SymbolAssociations);
        else if (x == IMPORT)
            the_hash_set = & (ImportAssociations);
        else if (x == META)
            the_hash_set = & (MetaAssociations);
        else if (x == SECTION)
            the_hash_set = & (SectionAssociations);
        else if (x == STRING)
            the_hash_set = & (StringAssociations);

        if (the_hash_set == NULL)
            return false;

        return the_hash_set->find(hash) != the_hash_set->end();;

    } 

    void update_hash_cache(  HASH_TYPE x, long hash){

        boost::container::set<long> *the_hash_set = NULL;
        if (x == SYMBOL)
            the_hash_set = & (SymbolAssociations);
        else if (x == IMPORT)
            the_hash_set = & (ImportAssociations);
        else if (x == META)
            the_hash_set = & (MetaAssociations);
        else if (x == SECTION)
            the_hash_set = & (SectionAssociations);
        else if (x == STRING)
            the_hash_set = & (StringAssociations);

        if (the_hash_set == NULL)
            return ;

        the_hash_set->insert(hash);

    } 

    void setSpecificBinaryInfo(RBinArch *arch){

        if (BClass == "PE32"){
            ImageBaseOffset = ((Pe32_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.ImageBase;
            EntryPoint = ((Pe32_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.AddressOfEntryPoint;   
            SizeOfImage = ((Pe32_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.SizeOfImage;
            TimeDateStamp = ((Pe32_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->file_header.TimeDateStamp;
        }else if (BClass == "PE64"){
            SizeOfImage = ((Pe64_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.SizeOfImage;
            ImageBaseOffset = ((Pe64_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.ImageBase;
            EntryPoint = ((Pe64_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->optional_header.AddressOfEntryPoint;
            TimeDateStamp = ((Pe64_r_bin_pe_obj_t *)arch->bin_obj)->nt_headers->file_header.TimeDateStamp;
        }else if (BClass == "ELF64"){
            EntryPoint = ((struct Elf32_r_bin_elf_obj_t *)arch->bin_obj)->ehdr.e_entry;
        }else if (BClass == "ELF64"){
            EntryPoint = ((struct Elf64_r_bin_elf_obj_t *)arch->bin_obj)->ehdr.e_entry;
        }

    }


    void readBinaryInfo(RBin *rBin){
        RBinInfo* rBinInfo = r_bin_get_info(rBin);
        boost::filesystem::path file_path;
        

        if (FileName == ""){
            file_path = boost::filesystem::path(rBinInfo->file);
            Path = file_path.string(); 
            FileName = file_path.filename().string();
            Extension = file_path.extension().string();
        }else{
            file_path= boost::filesystem::path(Path.c_str());
        }
        std::transform(Extension.begin(), Extension.end(), Extension.begin(), ::tolower);
        if (this->Extension.find(".") == 0){
            this->Extension.erase(0, 1);
        }

        
        FileType = rBinInfo->type != NULL ? rBinInfo->type : "";
        BClass = rBinInfo->bclass != NULL ? rBinInfo->bclass: "";
        RClass = rBinInfo->rclass != NULL ? rBinInfo->rclass : "";
        Architecture = rBinInfo->arch != NULL ? rBinInfo->arch : "";
        Machine =  rBinInfo->machine != NULL ? rBinInfo->machine: "";
        OS =  rBinInfo->os != NULL ? rBinInfo->os : "";
        Subsystem = rBinInfo->subsystem != NULL ? rBinInfo->subsystem : "";
        RPath = rBinInfo->rpath != NULL ? rBinInfo->rpath : "";
        Bits = rBinInfo->bits;
        IsBigEndian = rBinInfo->big_endian > 0;
        HasVirtualAddress = rBinInfo->has_va > 0;
        //RBinArch* arch = RBin->cur;
        // TODO: figure out how to get the base offset and the entrypoint
        //EntryPoint = r_bin_te_get_entrypoint (arch->bin_obj);
        RBinArch *arch = &(rBin->cur);
        setSpecificBinaryInfo(arch);

        
    }



public:
    ~File(){
    }

    File():
        HashId(0), Sha256(""), Md5(""), FuzzyHash(""), Architecture(""),
        Bits(0), RClass(""), BClass(""), Machine(""), OS(""), 
        FileType(""), Subsystem(""), RPath(""), IsBigEndian(false), 
        HasVirtualAddress(false), EntryPoint(0), 
        TimeDateStamp(0), Entropy(0.0), FractionalEntropy(0.0), 
        Path(""),FileName(""), Extension(""), ImageBaseOffset(0), SizeOfImage(0),
        FileSize(0){}
    
    File(RBin *rBin):
            HashId(0), Sha256(""), Md5(""), FuzzyHash(""), Architecture(""),
            Bits(0), RClass(""), BClass(""), Machine(""), OS(""), 
            FileType(""), Subsystem(""), RPath(""), IsBigEndian(false), 
            HasVirtualAddress(false), EntryPoint(0), 
            TimeDateStamp(0), Entropy(0.0), FractionalEntropy(0.0), 
            Path(""), FileName(""), Extension(""), ImageBaseOffset(0), SizeOfImage(0),
            FileSize(0){

        readBinaryInfo(rBin);
        std::string data; 
        get_file_data(Path, data);
        FileSize = data.size();

        Sha256 = calc_sha256(data);
        Md5 = calc_md5(data);
        HashId = long_hash_from_sha256_ascii(Sha256);

        Entropy = r_hash_entropy((ut8 *)data.c_str(), data.size());
        FractionalEntropy = r_hash_entropy_fraction((ut8 *)data.c_str(), data.size());

    }
    File(RCore *rCore):
            HashId(0), Sha256(""), Md5(""), FuzzyHash(""), Architecture(""),
            Bits(0), RClass(""), BClass(""), Machine(""), OS(""), 
            FileType(""), Subsystem(""), RPath(""), IsBigEndian(false), 
            HasVirtualAddress(false), EntryPoint(0), 
            TimeDateStamp(0), Entropy(0.0), FractionalEntropy(0.0), 
            Path(""), FileName(""), Extension(""),ImageBaseOffset(0),
            FileSize(0){

        RBin *rBin = r_core_get_bin(rCore);
        readBinaryInfo(rBin);
        
        std::string data; 
        get_file_data(Path, data);
        FileSize = data.size();

        Sha256 = calc_sha256(data);
        Md5 = calc_md5(data);
        HashId = long_hash_from_sha256_ascii(Sha256);
            
        Entropy = r_hash_entropy((ut8 *)data.c_str(), data.size());
        FractionalEntropy = r_hash_entropy_fraction((ut8 *)data.c_str(), data.size());

    }
    
    File(const std::string &filename):
            HashId(0), Sha256(""), Md5(""), FuzzyHash(""), Architecture(""),
            Bits(0), RClass(""), BClass(""), Machine(""), OS(""), 
            FileType(""), Subsystem(""), RPath(""), IsBigEndian(false), 
            HasVirtualAddress(false), EntryPoint(0), 
            TimeDateStamp(0), Entropy(0.0), FractionalEntropy(0.0), 
            Path(""), FileName(""), Extension(""),ImageBaseOffset(0){
        
        boost::filesystem::path file_path(filename.c_str());
        
        Path = file_path.string(); 
        FileName = file_path.filename().string();
        Extension = file_path.extension().string();
        
        std::string data; 
        get_file_data(Path, data);
        FileSize = data.size();
        
        RBin *rBin = r_bin_new();
        r_bin_load(rBin, Path.c_str(), R_FALSE);
        readBinaryInfo(rBin);
        r_bin_free(rBin);

        
        Sha256 = calc_sha256(data);
        Md5 = calc_md5(data);
        HashId = long_hash_from_sha256_ascii(Sha256);
            
        Entropy = r_hash_entropy((ut8 *)data.c_str(), data.size());
        FractionalEntropy = r_hash_entropy_fraction((ut8 *)data.c_str(), data.size());

    }
    /*void addSection(long hash, Section *section){
        
        if (section != NULL){
            update_hash_cache(SECTION, hash);
            long address = section->getRelativeVirtualAddress();            
            
            if(SectionAssociations.find(hash) != SectionAssociations.end()){
                Section *oSection = (Sections.find(address))->second;
                if ((*oSection) == (*section)){
                    // nothing to be don, just return
                    return;
                }
                std::cerr << "Section already exists in Section, but they are not the same " << std::endl;
                address += 4;
            }

            Sections[address] = section;    
        }

        
    }*/

    bool operator==(File &file){
        return file.getSha256() == Sha256;
    }
    bool operator!=(File &file){
        return file.getSha256() != Sha256;
    }
    void addMetaAssociation(long hash){
        update_hash_cache(META, hash);
    }        
    void addSectionAssociation(long hash){
        update_hash_cache(SECTION, hash);
    }
    void addSymbolAssociation(long hash){
        update_hash_cache(SYMBOL, hash);
    }        
    void addStringAssociation(long hash){
        update_hash_cache(STRING, hash);
    }
    void addImportAssociation(long hash){
        update_hash_cache(IMPORT, hash);
    }        

    bool hasMetaAssociation(long hash){
        return hasAssociation(META, hash);
    }        
    bool hasSectionAssociation(long hash){
        return hasAssociation(SECTION, hash);
    }
    bool hasSymbolAssociation(long hash){
        return hasAssociation(SYMBOL, hash);
    }        
    bool hasStringAssociation(long hash){
        return hasAssociation(STRING, hash);
    }
    bool hasImportAssociation(long hash){
        return hasAssociation(IMPORT, hash);
    }
    
    void setRelativeVirtualAddress(long rva){
        ImageBaseOffset = rva;
    }
    long getRelativeVirtualAddress() const  { return ImageBaseOffset;}
    long getHashId() const { return HashId; }
    std::string getSha256() const { return Sha256; }
    std::string getMd5() const { return Md5;}
    std::string getFuzzyHash() const { return FuzzyHash;}
    std::string getArchitecture() const { return Architecture;}
    int getBits() const { return Bits;}
    std::string getRClass() const { return RClass;}
    std::string getBClass() const { return BClass;}
    std::string getMachine() const { return Machine;}
    std::string getOS() const { return OS;}
    std::string getFileType() const { return FileType;}
    std::string getSubsystem() const { return Subsystem;}
    bool isBigEndian() const { return IsBigEndian;}
    bool hasVirtualAddress() const  {return HasVirtualAddress;}
    std::string getRPath() const  {return RPath;}
    long getEntryPoint() const  {return EntryPoint;}
    long getTimeDateStamp() const  {return TimeDateStamp;}

    double getEntropy() const  {return Entropy;}
    double getFractionalEntryPoint() const  {return FractionalEntropy;}
    std::string getFileName() const  {return FileName;}
    std::string getExtension() const  {return Extension;}

    long getSizeOfImage() const { return SizeOfImage;}
    long getImageBaseOffset() const { return ImageBaseOffset;}
    long getFileSize() const { return FileSize;}
    /*std::string getFullPath() const {
        boost::filesystem::path dir (Path);
        boost::filesystem::path file (FileName);
        boost::filesystem::path full_path = dir / file;
        return full_path.string();
    }*/

    std::string summarize() const{
        std::stringstream ss_out;
        
        ss_out << "File Summary: " << std::endl;
        ss_out << "Filename: " << FileName << std::endl;
        ss_out << "Extension: " << Extension << std::endl;
        ss_out << "Path: " << Path << std::endl;

        ss_out << "FileType: " << FileType << std::endl;
        ss_out << "FileBClass: " << BClass << std::endl;
        ss_out << "FileRClass: " << RClass << std::endl;
        ss_out << "Machine: " << Machine << std::endl;
        ss_out << "OS: " << OS << std::endl;
        ss_out << "Subsystem: " << Subsystem << std::endl;
        ss_out << "RPath: " << RPath << std::endl;
        ss_out << "Bits: " << Bits << std::endl;
        ss_out << "IsBigEndian: " << IsBigEndian << std::endl;
        ss_out << "HasVa: " << HasVirtualAddress << std::endl;
        ss_out << "EntryPoint: " << std::hex  << EntryPoint << std::endl;
        ss_out << "ImageBaseOffset: " << std::hex << ImageBaseOffset << std::endl;
        ss_out << "SizeOfImage: " << std::hex << SizeOfImage << std::endl;
        ss_out << "FileSize: " << std::hex << FileSize << std::endl;

        // set ios::hex
        ss_out << "HashId: " << HashId << std::endl;
        ss_out << "Sha256: " << Sha256 << std::endl;
        ss_out << "Md5: " << Md5 << std::endl;
        ss_out << "Fuzzy: " << FuzzyHash << std::endl;
        ss_out << "Architecture: " << Architecture << std::endl;
        
        
        ss_out << "Entropy: " << Entropy << std::endl;
        ss_out << "FractionalEntropy: " << FractionalEntropy << std::endl;
        ss_out << "TimeDateStamp: " << TimeDateStamp << std::endl;
        ss_out << "EntryPoint: " << EntryPoint << std::endl;
        

        std::string out = ss_out.str();
        return out;
    }


    
    bool fromCSV(std::string &input){
        
        Poco::StringTokenizer tokens(input, ",");
        Poco::StringTokenizer header_tokens(summarizeCSVHeader(), ",");

        ut32 max_len = header_tokens.count();

        if (max_len != tokens.count() ) {
            // FIXME: throw an exception
            std::cerr << "Invalid string!!!!" << std::endl;
            return false;
        }
        /*
        for (ut32 i = 0; i < max_len && i < tokens.count(); i++){
            std::cout << "(" << i << ") " << header_tokens[i] << ": " << tokens[i] << std::endl;
        }*/

        FileName = tokens[0];
        Extension = tokens[1].size() > 0 ? tokens[1] : std::string("");
        Path = tokens[2];

        FileType = tokens[3];
        BClass = tokens[4];
        RClass = tokens[5];
        Machine = tokens[6];
        OS = tokens[7];
        Subsystem = tokens[8];
        RPath = tokens[9];
        Bits = Poco::NumberParser::parseHex64(tokens[10]);
        IsBigEndian = (bool) Poco::NumberParser::parse(tokens[11]);
        HasVirtualAddress = (bool) Poco::NumberParser::parse(tokens[12]);
        EntryPoint = Poco::NumberParser::parseHex64(tokens[13]);
        ImageBaseOffset = Poco::NumberParser::parseHex64(tokens[14]);
        SizeOfImage = Poco::NumberParser::parseHex64(tokens[15]);
        FileSize = Poco::NumberParser::parseHex64(tokens[16]);
        //std::cout << "something is not working right :/" << std::endl;
        // set ios::hex
        HashId = Poco::NumberParser::parseHex64(tokens[17]);
        Sha256 = tokens[18];
        Md5 = tokens[19];
        FuzzyHash = tokens[20].size() > 0 ? tokens[20] : std::string("");;
        Architecture = tokens[21];
        //std::cout << "something is not working right :/" << std::endl;
        
        Entropy = Poco::NumberParser::parseFloat(tokens[22]);
        FractionalEntropy = Poco::NumberParser::parseFloat(tokens[23]);;
        TimeDateStamp = Poco::NumberParser::parseHex64(tokens[24]);

        return true;
    }

    std::string summarizeCSV() const{
        std::stringstream ss_out;
        
        ss_out <<  std::hex;

        ss_out << FileName << ",";
        ss_out << Extension << ",";
        ss_out << Path << ",";

        ss_out << FileType << ",";
        ss_out << BClass << ",";
        ss_out << RClass << ",";
        ss_out << Machine << ",";
        ss_out << OS << ",";
        ss_out << Subsystem << ",";
        ss_out << RPath << ",";
        ss_out << Bits << ",";
        ss_out << IsBigEndian << ",";
        ss_out << HasVirtualAddress << ",";
        ss_out << EntryPoint << ",";
        ss_out << ImageBaseOffset << ",";
        ss_out << SizeOfImage << ",";
        ss_out << FileSize << ',';

        // set ios::hex
        ss_out << HashId << ",";
        ss_out << Sha256 << ",";
        ss_out << Md5 << ",";
        ss_out << FuzzyHash << ",";
        ss_out << Architecture << ",";
        
        
        ss_out << Entropy << ",";
        ss_out << FractionalEntropy << ",";
        ss_out << TimeDateStamp;

        std::string out = ss_out.str();
        return out;
    }

    std::string summarizeCSVHeader() const{
        std::stringstream ss_out;
        
        ss_out <<  std::hex;

        ss_out << "Filename,";// << FileName << std::endl;
        ss_out << "Extension,";// << Extension << std::endl;
        ss_out << "Path,";// << Path << std::endl;

        ss_out << "FileType,";// << FileType << std::endl;
        ss_out << "FileBClass,";// << BClass << std::endl;
        ss_out << "FileRClass,";// << RClass << std::endl;
        ss_out << "Machine,";// << Machine << std::endl;
        ss_out << "OS,";// << OS << std::endl;
        ss_out << "Subsystem,";// << Subsystem << std::endl;
        ss_out << "RPath,";// << RPath << std::endl;
        ss_out << "Bits,";// << Bits << std::endl;
        ss_out << "IsBigEndian,";// << IsBigEndian << std::endl;
        ss_out << "HasVa,";// << HasVirtualAddress << std::endl;
        ss_out << "EntryPoint,";// << std::hex  << EntryPoint << std::endl;
        ss_out << "ImageBaseOffset,";// << std::hex << ImageBaseOffset << std::endl;
        ss_out << "SizeOfImage,";
        ss_out << "FileSize,";
    

        // set ios::hex
        ss_out << "HashId,";// << HashId << std::endl;
        ss_out << "Sha256,";// << Sha256 << std::endl;
        ss_out << "Md5,";// << Md5 << std::endl;
        ss_out << "Fuzzy,";// << FuzzyHash << std::endl;
        ss_out << "Architecture,";// << Architecture << std::endl;
        
        
        ss_out << "Entropy,";// << Entropy << std::endl;
        ss_out << "FractionalEntropy,";// << FractionalEntropy << std::endl;
        ss_out << "TimeDateStamp";// << TimeDateStamp << std::endl;


        std::string out = ss_out.str();
        return out;
    }
    template<class Action>
    void persist(Action& a)
    {

        dbo::field(a, FileName, "FileName");
        dbo::field(a, Extension, "Extension");
        dbo::field(a, Path, "Path");
        

        dbo::field(a, HashId, "HashId");
        dbo::field(a, Md5, "Md5");
        dbo::field(a, Sha256, "Sha256");
        dbo::field(a, FuzzyHash, "FuzzyHash");

        dbo::field(a, FileType, "FileType");
        dbo::field(a, Architecture, "Architecture");
        dbo::field(a, Bits, "Bits");
        dbo::field(a, RPath, "RPath");
        dbo::field(a, RClass, "RClass");
        dbo::field(a, BClass, "BClass");
        dbo::field(a, Machine, "Machine");
        dbo::field(a, OS, "OS");
        dbo::field(a, Subsystem, "Subsystem");

        dbo::field(a, IsBigEndian, "IsBigEndian");
        dbo::field(a, HasVirtualAddress, "HasVirtualAddress");

        dbo::field(a, EntryPoint, "EntryPoint");
        dbo::field(a, ImageBaseOffset, "ImageBaseOffset");
        dbo::field(a, SizeOfImage, "SizeOfImage");
        dbo::field(a, FileSize, "FileSize");

        dbo::field(a, TimeDateStamp, "TimeDateStamp");

        dbo::field(a, Entropy, "Entropy");
        dbo::field(a, FractionalEntropy, "FractionalEntropy");

    }

};
#endif
