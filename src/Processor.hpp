#ifndef RADARE_PROCESSOR_H
#define RADARE_PROCESSOR_H

#include <r_bin.h>
#include <r_lib.h>
#include <r_list.h>
#include <r_hash.h>
#include <r_core.h>


#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>


#include <boost/container/map.hpp>
#include <boost/regex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/random.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/date_time.hpp>

#include "tspriorityqueue.hpp"
#include "ProcessorWorker.hpp"
#include "File.hpp"

namespace Processing{

class Processor: 
    public boost::enable_shared_from_this<Processor>,
    private boost::noncopyable
{


    UniqueTSPQueue<std::string>& filenames_queue_;

    boost::interprocess::interprocess_upgradable_mutex completed_files_mutex_;
    boost::container::map<long, File *> completed_files_;
    boost::container::map<long, bool> file_is_being_used_;
    
    unsigned short max_pending_threads_;
    
    bool continueToRun_;


    boost::interprocess::interprocess_upgradable_mutex tasked_threads_mutex_;
    boost::container::map<long, ProcessorWorker*> tasked_threads_;    
    long currentProcessNumber_;


    boost::condition_variable pending_tasks_cnd_;
    boost::mutex processing_threads_mutex_;
    long running_threads_;
    
    //Processor& Processor(const Processor&){}
    //Processor& operator=(const Processor&){}
    bool doFileDelete(long fileId);
    const File * doGetFile(long fileId);
    void doFileAdd(long fileId, File * f);
    bool doDeleteTask(long taskId);

protected:
    log4cxx::LoggerPtr myLogger;

public:
    Processor(unsigned short max_threads, UniqueTSPQueue<std::string>& filenames_queue):
        filenames_queue_(filenames_queue),
        completed_files_mutex_(),
        completed_files_(),
        file_is_being_used_(),
        max_pending_threads_(max_threads),
        continueToRun_(false),
        tasked_threads_mutex_(),
        tasked_threads_(),
        currentProcessNumber_(0),
        pending_tasks_cnd_(),
        processing_threads_mutex_(),
        running_threads_(0){ 
            this->myLogger = getLogger(std::string("Processor"));
        }

    /*Processor(unsigned short max_threads, std::vector<std::string>& filenames_vector):
        filenames_queue_(),
        completed_files_mutex_(),
        completed_files_(),
        file_is_being_used_(),
        max_pending_threads_(max_threads),
        continueToRun_(false),
        tasked_threads_mutex_(),
        tasked_threads_(),
        currentProcessNumber_(0),
        pending_tasks_cnd_(),
        processing_threads_mutex_(){ 

            std::vector<std::string>::iterator iterVecFilenames = filenames_vector.begin();

            for(;iterVecFilenames != filenames_vector.end(); iterVecFilenames++){
                filenames_queue_.push(*iterVecFilenames);
            }

        }*/


    void handleFileParsingCompletion(ProcessorWorker &radareProcessorWorker);

    void releaseAccessToCompletedFile(long fileId);
    void releaseAccessToCompleteFilesById(std::vector<long> * fileIds);

    void waitThreadToComplete();
    void clearCompletedFiles();
    long getFirstCompletedFileId();
    std::vector<long> * getCompletedFileIds();
    const File * getCompletedFileById(long id);
    std::vector<const File*>* getCompletedFilesById(std::vector<long> * fileIds);
    bool deleteCompletedFileById(long id);
    std::vector<bool> * deleteCompletedFilesById(std::vector<long> * fileIds);

    bool canStartMoreThreads();

    std::string startOneWorker();

    std::vector<std::string> * startMultipleWorkers();

    long getCurrentProcessorThreadsCnt();

    long getMaxProcessorThreadsCnt();

    long getRemainingFilenameCnt();

    std::string processNextName();
    void cleanupDeadThreads();

    boost::container::map<long, File *> & getProcessedFiles(){
        return this->completed_files_;
    }
    void summarizeAllFiles();
    ~Processor();

};
}
#endif
