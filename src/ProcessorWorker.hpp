#ifndef RADARE_PROCESSOR_WORKER_H
#define RADARE_PROCESSOR_WORKER_H


#include <string>
#include <iostream>

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include "File.hpp"
#include "Section.hpp"
//#include "Processor.hpp"

//    Worker w(3);
//    boost::thread workerThread(&Worker::processQueue, &w, 2);
namespace Processing{
class Processor;

class ProcessorWorker:
    public boost::enable_shared_from_this<ProcessorWorker>,
    private boost::noncopyable

{

    
    File * mFile_;
    Processor &processor_;
    bool taskCompleted;
    bool ownResult_;
    long task_id_;
    boost::thread * m_Thread;
    
    //ProcessorWorker& ProcessorWorker(const ProcessorWorker&){}
    //ProcessorWorker& operator=(const ProcessorWorker&){}
protected:
    log4cxx::LoggerPtr myLogger;
    
public:
    ProcessorWorker(Processor &processor):
        processor_(processor), 
        taskCompleted(false),
        ownResult_(true),
        task_id_(0),
        m_Thread(NULL){
            this->myLogger = getLogger(std::string("ProcessorWorker"));
        }
    
/*    ProcessorWorker():
        processor_(NULL), 
        taskCompleted(false),
        ownResult_(true),
        task_id_(0){}
*/
    ~ProcessorWorker(){
        if(ownResult_)
            delete mFile_;
    }

    void setTaskId(long task_id);
    long getTaskId();
    void start(std::string &filename);
    void join();
    bool isAlive();

    void releaseResultOwnerShip();


    void processFile(std::string &filename);
    
    File *getFile();        
};
}
#endif
