/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
#include <Wt/Dbo/Dbo>
#include <Wt/WLogger>
#include <string>
#include <Wt/Dbo/Exception>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>

#include <stdlib.h> 
#include <cstring>

#include <algorithm>

#include <boost/filesystem.hpp>

#include <postgresql/libpq-fe.h>
#include <vector>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "util.hpp"
#include "dbo_orm_base.hpp"
#include "dbo_orm_defs_radare.hpp"

namespace dbo = Wt::Dbo;

namespace file_analysis{

boost::container::set<long> IMPORT_HASHES;
boost::container::set<long> SYMBOL_HASHES;
boost::container::set<long> FILES_HASHES;
boost::container::set<long> SECTIONS_HASHES;
boost::container::set<long> STRINGS_HASHES;
boost::container::set<long> METAS_HASHES;

boost::container::set<long> SYMBOLS_ASSOC_HASHES;
boost::container::set<long> IMPORTS_ASSOC_HASHES;
boost::container::set<long> SECTIONS_ASSOC_HASHES;
boost::container::set<long> STRINGS_ASSOC_HASHES;
boost::container::set<long> METAS_ASSOC_HASHES;

volatile bool SRAND_CALLED = false;

void ORMBase::init_hash_cache(  HASH_CACHE_TYPE x){
    std::string sql = "select HashId from ", table = "";
    boost::container::set<long> *the_hash_set = NULL;

    if (x == SYMBOLS){
        table = SYMBOLS_TABLE;
        the_hash_set = & (SYMBOL_HASHES);
    }
    else if (x == IMPORTS){
        table = IMPORTS_TABLE;
        the_hash_set = & (IMPORT_HASHES);
    }
    else if (x == FILES){
        table = FILES_TABLE;
        the_hash_set = & (FILES_HASHES);
    }
    else if (x == SECTIONS){
        table = SECTIONS_TABLE;
        the_hash_set = & (SECTIONS_HASHES);
    }
    else if (x == IMPORTS_ASSOC){
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
        table = IMPORTS_ASSOC_TABLE;
    }
    else if (x == SYMBOLS_ASSOC){
        the_hash_set = & (SYMBOLS_ASSOC_HASHES);
        table = SYMBOLS_ASSOC_TABLE;
    }
    else if (x == SECTIONS_ASSOC){
        the_hash_set = & (SECTIONS_ASSOC_HASHES);
        table = SECTIONS_ASSOC_TABLE;
    }

    std::string teh_query = sql + table + std::string(";");

    dbo::Transaction transaction_get_hashes(session);
    if (the_hash_set == NULL)
        return;
     
    Wt::Dbo::Query<long> query = session.query< long >(teh_query);
    

    Wt::Dbo::collection<long> results = query.resultList();
    Wt::Dbo::collection<long>::iterator it = results.begin();

    for (; it != results.end(); it++){
        the_hash_set->insert(*it);
    }


    std::stringstream ss_log;
    ss_log << "[+] Inserted: " << results.size() << " elements into the "<< table << " set."; 
    LOG4CXX_TRACE (this->myLogger, ss_log.str());


} 

bool ORMBase::check_hash_cache(  HASH_CACHE_TYPE x, long hash){
    boost::container::set<long> *the_hash_set = NULL;
    if (x == SYMBOLS)
        the_hash_set = & (SYMBOL_HASHES);
    else if (x == IMPORTS)
        the_hash_set = & (IMPORT_HASHES);
    else if (x == FILES)
        the_hash_set = & (FILES_HASHES);
    else if (x == IMPORTS_ASSOC)
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
    else if (x == SYMBOLS_ASSOC)
        the_hash_set = & (SYMBOLS_ASSOC_HASHES);

    if (the_hash_set == NULL)
        return false;

    return the_hash_set->find(hash) != the_hash_set->end();;

} 

void ORMBase::update_hash_cache(  HASH_CACHE_TYPE x, long hash){

    boost::container::set<long> *the_hash_set = NULL;
    if (x == SYMBOLS)
        the_hash_set = & (SYMBOL_HASHES);
    else if (x == IMPORTS)
        the_hash_set = & (IMPORT_HASHES);
    else if (x == FILES)
        the_hash_set = & (FILES_HASHES);
    else if (x == SECTIONS)
        the_hash_set = & (SECTIONS_HASHES);
    else if (x == SECTIONS_ASSOC)
        the_hash_set = & (SECTIONS_ASSOC_HASHES);
    else if (x == IMPORTS_ASSOC)
        the_hash_set = & (IMPORTS_ASSOC_HASHES);
    else if (x == SYMBOLS_ASSOC)
        the_hash_set = & (SYMBOLS_ASSOC_HASHES);

    if (the_hash_set == NULL)
        return ;

    the_hash_set->insert(hash);

} 

void ORMBase::process_file(RCore *rCore){

}    
void ORMBase::process_sections(RCore *rCore, dbo::ptr<File> dbo_file){

}
void ORMBase::process_imports(RCore *rCore, dbo::ptr<File> dbo_file){

}
void ORMBase::process_symbols(RCore *rCore, dbo::ptr<File> dbo_file){

}
void ORMBase::process_strings(RCore *rCore, dbo::ptr<File> dbo_file){

}


/*
void ORMBase::process_symbols( dbo::ptr<File> dbo_file,  RCore *rCore){
    


    std::string DllName = pefilePtr->FileName;

    dbo::Transaction process_exports_tran(this->session);

    std::vector< dbo::ptr<Symbol> > exportsVector;
    
    unsigned int numberOfSymbols =  exp.getNumberOfFunctions();

    boost::container::set<long> dbos_to_query;

    if (numberOfSymbols == 0)
        return;
    
    //dbo::Transaction transaction_add_export(session);
    // create check and update the exports    
    for (unsigned int i=0;i<exp.getNumberOfFunctions();i++)
    {
        std::string name =  exp.getFunctionName(i);
        Symbol *exportPtr = new Symbol(name, DllName);

        if (check_hash_cache(SYMBOLS, exportPtr->hash_id)){
            dbos_to_query.insert(exportPtr->hash_id);
            delete exportPtr;
        }else{
            dbo::ptr<Symbol> dbo_export = session.add(exportPtr);
            update_hash_cache(SYMBOLS, exportPtr->hash_id);
            exportsVector.push_back(dbo_export);
        }
        
    }
    
    LOG4CXX_TRACE (this->myLogger, std::string("Querying DB for existing exports."));
    boost::container::set<long>::iterator it;
    for(it = dbos_to_query.begin(); it != dbos_to_query.end(); it++){
        dbo::ptr<Symbol> dbo_export = session.find<Symbol>().where("hash_id = ?").bind(*it);
        exportsVector.push_back(dbo_export);
    }

    // update the exports association table
    LOG4CXX_TRACE (this->myLogger, std::string("Creating the export association in the DB, if they dont exist"));
    std::vector< dbo::ptr<Symbol> >::iterator it_exports = exportsVector.begin();
    for (; it_exports != exportsVector.end(); it_exports++){
        SymbolAssociations *exportAssocPtr = new SymbolAssociations(*(*it_exports), *pefilePtr);
        if (!check_hash_cache(SYMBOLS_ASSOC, exportAssocPtr->hash_id)){
            dbo::ptr<SymbolAssociations> dbo_export_assoc = session.add(exportAssocPtr);
            dbo_export_assoc.modify()->export_relation = *it_exports;
            dbo_export_assoc.modify()->pefile = dbo_pefile;
            update_hash_cache(SYMBOLS_ASSOC, exportAssocPtr->hash_id);
        }   
    }
    process_exports_tran.commit();
    this->session.flush();
    //transaction_add_export.commit();
    LOG4CXX_TRACE (this->myLogger, std::string("Completed saving the exports to the DB"));
    
}



void ORMBase::process_file(RCore *rCore){
    
    /*
    File *pefilePtr;
    dbo::ptr<File> dbo_pefile;
    std::stringstream ss_log;

    ss_log << "[=] Processing file : " << file.getFileName();
    LOG4CXX_DEBUG (this->myLogger, ss_log.str());
    ss_log.str("");

    pefilePtr = new File(file);
        
    dbo::Transaction create_pefile_tran(session);

    if (check_hash_cache(FILES, pefilePtr->hash_id)){
        dbo_pefile = session.find<File>().where("hash_id = ?").bind(pefilePtr->hash_id);
        
        if (pefilePtr->Sha256 == dbo_pefile->Sha256)
        {
            delete pefilePtr;
            return;
        }
        
        ss_log << "[-] Found a duplicate hash for 32-bit : " << pefilePtr->FileName;
        LOG4CXX_INFO (this->myLogger, ss_log.str());
        ss_log.str("");
    }else{
        ss_log  << "[+] Added 32-bit ("<< pefilePtr->hash_id << ") " << pefilePtr->FileName;
        LOG4CXX_TRACE (this->myLogger, ss_log.str());
        ss_log.str("");

        update_hash_cache(FILES, pefilePtr->hash_id);

        dbo_pefile = session.add(pefilePtr);
        this->process_sections<32>(file, pefilePtr->hash_id);
        
        ss_log << "[+] Completed Sections processing for: " << dbo_pefile->FileName;
        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");


    }


    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash    
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Enumerating the exports"));
    if (pefilePtr->NumberOfSymbols > 0){
        // Since this file performs the exports, we will use it as the DllName
        PeLib::SymbolDirectory& exp = dynamic_cast<PeLib::PeFile &>(file).expDir();
        process_exports(dbo_pefile, pefilePtr, exp);
    }
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Finished enumerating the exports"));
    
    // add Imports to the DB and get a backed DBO,
    // if the import exist in the DB simply pull it out
    // by the cached hash
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Enumerating the imports"));
    if (pefilePtr->NumberOfImportFiles > 0){
        process_imports<32>(file, dbo_pefile, pefilePtr);
    }
    LOG4CXX_DEBUG (this->myLogger, std::string("[+] Finished enumerating the imports"));
    create_pefile_tran.commit();
    this->session.flush();

    
    
}
*/

void ORMBase::scan_and_process_directory_of_exes( std::string & start_dir){
    
    boost::filesystem::path top_dir(start_dir);
    
    std::string sha256_hash = "";
    std::string md5_hash = "";
    std::stringstream ss_log;

    ss_log << std::hex << rand();
    this->badpefile_log = "files_failed_read_";
    this->badpefile_log += ss_log.str();
    this->badpefile_log += std::string(".log");
    ss_log.str("");
    ss_log <<  std::dec;
    
    // quick header indicating the directory scanned name.
    std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
    outfile << "# Scanning dir: " << start_dir << std::endl; 
    outfile.close();

    unsigned int files_processed = 0;
    for ( boost::filesystem::recursive_directory_iterator end, dir(top_dir); 
        dir != end; ++dir, ++files_processed) {
        std::string the_path = (*dir).path().native();
        if (the_path.find("/Microsoft/CryptnetUrlCache/Content/") != std::string::npos){
            std::string str_log = std::string( "[-] Skipping " ) + the_path + std::string(".  Might want to check it out.");
            LOG4CXX_WARN (this->myLogger, str_log);

        }

        if (boost::filesystem::is_directory(the_path))
            continue;

        
        RCore* rCore = r_core_new();
        try{
            if (rCore == NULL){
                // throw new exception

            }


            struct r_core_file_t *rCoreFile = r_core_file_open(rCore, file.c_str(), 0, 0);
            
            if (rCoreFile == NULL || rCore == NULL){
                std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
                outfile << the_path << std::endl; 
                outfile.close();

                
                ss_log << "[-] Unable to parse filename (openPeFile returned NULL): " << the_path; 
                LOG4CXX_WARN (this->myLogger, ss_log.str());
                ss_log.str("");
                
                continue;    
            }

            if (r_core_bin_load(rCore, file.c_str())){
                this->process_file(rCore);
            }else{
                r_core_free(rCore);
                rCore = NULL;
                // throw new exception
            }
            
            ss_log << "[+] Processed ("<< files_processed << "): "<< the_path;
            LOG4CXX_TRACE (this->myLogger, ss_log.str());
            ss_log.str("");
        }catch(std::exception& e){
            std::ofstream outfile(this->badpefile_log.c_str(), std::ios::app );
            outfile << the_path << std::endl; 
            outfile.close();

            ss_log << "[-] Unable to parse filename: " << the_path << std::endl; 
            ss_log << "\t[=] Got the following exception:\n" << e.what();

            LOG4CXX_WARN (this->myLogger, ss_log.str());
            ss_log.str("");
        }

        if (rCore != NULL)
            r_core_free(rCore);

        if (files_processed % 100 == 0){
            ss_log << "[+] Processed ("<< files_processed << ") files.";
            LOG4CXX_INFO (this->myLogger, ss_log.str());
            ss_log.str("");
        }
    }
}




void ORMBase::init_database(){
    
    try {
        dbo::Transaction setup(session);

        std::string str_log;
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + FILES_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<File>(FILES_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + IMPORTS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Import>(IMPORTS_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + SYMBOLS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Symbol>(SYMBOLS_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + SECTIONS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(SECTIONS_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + STRINGS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(STRINGS_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + METAS_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(METAS_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + IMPORTS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<ImportAssociations>(IMPORTS_ASSOC_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + SYMBOLS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<SymbolAssociations>(SYMBOLS_ASSOC_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + SECTIONS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(SECTIONS_ASSOC_TABLE.c_str());
        
        str_log = std::string("[=] Mapping the ORM objects to table: ") + STRINGS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(STRINGS_ASSOC_TABLE.c_str());

        str_log = std::string("[=] Mapping the ORM objects to table: ") + METAS_ASSOC_TABLE;
        LOG4CXX_TRACE(this->myLogger, str_log);
        session.mapClass<Section>(METAS_ASSOC_TABLE.c_str());

        str_log = std::string("[=] Creating the tables.");
        LOG4CXX_TRACE(this->myLogger, str_log);
        this->session.createTables();
        
        setup.commit();
        
        str_log = std::string("[+] Completed initialization of the tables.");
        LOG4CXX_INFO(this->myLogger, str_log);
        
    }catch(Wt::Dbo::Exception & e){

        std::string str_log = std::string("[=] Caught DB exception during initialization: ")+ e.what();
        LOG4CXX_WARN(this->myLogger, str_log);
        

        str_log = std::string("[=] Trying to use an existing database.");
        LOG4CXX_INFO(this->myLogger, str_log);
        //init_hash_cache(SYMBOLS);
        //init_hash_cache(IMPORTS);
        //init_hash_cache(FILES);
        //init_hash_cache(SECTIONS);
        //init_hash_cache(IMPORTS_ASSOC);
        //init_hash_cache(SYMBOLS_ASSOC);

        str_log = "[+] Completed caching ORM DBO hash keys for processing.";
        LOG4CXX_INFO(this->myLogger, str_log);
        
        // need to cache all of the object hashes to ensure
        // expedited transactions and avoid creating objects
        // that already exist
    }
}
/*
void ORMBase::process_sections(RCore *rCore, dbo::ptr<File> & file){
        std::stringstream ss_log (std::stringstream::in | std::stringstream::out);



        /*
        
        std::string filename = pef.getFileName();
        //dbo::Transaction transaction(session);

        for (int i=0;i<peh.getNumberOfSections();i++)
        {
            
            std::string sectionName = peh.getSectionName(i);
            ss_log << "[=] Processing Section: " << sectionName;
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            uint64_t vAddr = peh.getVirtualAddress(i);
            PeLib::dword vSize = peh.getVirtualSize(i),
                 rSize = peh.getSizeOfRawData(i),
                 ptrData = peh.getPointerToRawData(i),
                 characteristics = peh.getCharacteristics();

            uint64_t dataSz =  0;

            ss_log << "[=] Calulating Section Entropy.";
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            //XXX Disabled the Entropy calculation to speed up analysis
            float *entropy;// = new float;
        *entropy = 0.0;
            if(rSize == 0)
            {
                // XXX Disabled here
                entropy = calcEntropy(filename, ptrData, vSize);
                if (entropy == NULL)
                {
                    ss_log << "[-] Failed to calulate Section Entropy.";
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                }                 
                
            }
            else
            {
                // XXX Disabled here
                entropy = calcEntropy(filename, ptrData, rSize);
                if (entropy == NULL)
                {
                    ss_log << "[-] Failed to calulate Section Entropy.";
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                }                 

            }

            if (entropy == NULL)
            {   
                entropy = new float;
                *entropy = 0.0;
            }                 



            ss_log << "Calulated Section Entropy: " << *entropy;
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            Section *section = new Section(dbo_pefile->FileName,
                                            sectionName, vSize, rSize, 
                                            vAddr, *entropy, characteristics);
            
            ss_log << "Section: " << (section->SectionName) << " for PE: ";
            ss_log << (dbo_pefile->FileName) << " has hash_id: " << (section->hash_id);
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            dbo::ptr<Section> dbo_section = session.add(section);
            dbo_section.modify()->pefile = dbo_pefile;

            if (dbo_pefile->EntryPoint <= vAddr &&
                 dbo_pefile->EntryPoint <= (vAddr + vSize))
            {
                dbo_pefile.modify()->SectionNameOEP =  section->SectionName;
                //dbo_section.modify()->SectionOEP =  dbo_pefile;
            }
  
            try
            {

                ss_log << "[+] Completed Processing Section: " << section->SectionName;
                
                LOG4CXX_DEBUG(this->myLogger, ss_log.str());
                ss_log.str("");            
                
            }
            catch(std::exception & e)
            {
                    
                ss_log << "[-] Failed to add Section: " << section->SectionName << std::endl;
                ss_log << e.what();             
                LOG4CXX_WARN(this->myLogger, ss_log.str());
                ss_log.str("");

            }

            delete entropy;

        }
        //transaction.commit();

        ss_log << "[+] Completed Sections processing for: " << filename;
        //ss_log << "Adding sections to: " << dbo_pefile->hash_id ;

        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");
            
    }
}



void ORMBase::process_imports(RCore *rCore, dbo::ptr<File> & file){
/*

        dbo::Transaction process_imports_tran(this->session);

        std::vector< dbo::ptr<Import> > dbo_import_vector;

        std::stringstream ss_log (std::stringstream::in | std::stringstream::out);
        boost::container::set<long> dbos_to_query;

        const PeLib::ImportDirectory<bits>& imp = static_cast<PeLib::PeFileT<bits>&>(pef).impDir();

        int numberOfImports = imp.getNumberOfFiles(PeLib::OLDDIR);
        
        if (numberOfImports == 0)
            return;
        
        ss_log << "process_imports: Current PEFile.hash_id: " << dbo_pefile->hash_id;    
        LOG4CXX_TRACE(this->myLogger, ss_log.str());
        ss_log.str("");
        //dbo::Transaction transaction_add_import_associations(this->session);

            for (unsigned int i=0;i<numberOfImports;i++)
            {        
                
                std::string DllName = imp.getFileName(i, PeLib::OLDDIR);                    
                for (unsigned int j=0;j<imp.getNumberOfFunctions(i, PeLib::OLDDIR);j++)
                {
                    std::string functionName = imp.getFunctionName(i, j, PeLib::OLDDIR);

                    if (functionName == std::string("")){
                        std::stringstream hint;
                        hint << std::hex << imp.getFunctionHint(i, j, PeLib::OLDDIR);
                        functionName = hint.str();
                    }

                    Import *importPtr = new Import(functionName, DllName);

                    if (importPtr == NULL){
                        ss_log << "importPtr is NULL, skipping";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                        continue;                        
                    }

                    if (this->check_hash_cache(IMPORTS, importPtr->hash_id)){
                        ss_log << "Import("<<importPtr->hash_id<<") does exist: " << DllName << ":" << functionName;    
                        LOG4CXX_TRACE(this->myLogger, ss_log.str());
                        ss_log.str("");
                        dbos_to_query.insert(importPtr->hash_id);
                        delete importPtr;

                    }else{
                        ss_log << "Import("<<importPtr->hash_id<<") does not exist: " << DllName << ":" << functionName;    
                        LOG4CXX_TRACE(this->myLogger, ss_log.str());
                        ss_log.str("");

                        dbo::ptr<Import> dbo_import = session.add(importPtr);
                        this->update_hash_cache(IMPORTS, importPtr->hash_id);
                        dbo_import_vector.push_back(dbo_import);
                    }
                    
                }        
            }
            ss_log << "[=] Querying DB and creating ImportAssociations if they don't exist.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");
            
            
            for(boost::container::set<long>::iterator it = dbos_to_query.begin(); 
                it != dbos_to_query.end(); it++){
                
                ss_log << "Querying hash_id = " << *it;    
                LOG4CXX_TRACE(this->myLogger, ss_log.str());
                ss_log.str("");

                dbo::ptr<Import> dbo_import = this->session.find<Import>().where("hash_id = ?").bind(*it);
                if (dbo_import){
                    // do nothing
                }else
                {
                    ss_log << "[-] dbo_import ("<< *it << ") was NULL, so it is probably not in the DB.";    
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");
                    continue;                    
                }


                ImportAssociations *importAssocPtr = new ImportAssociations(*dbo_import, *pefilePtr);
                if (importAssocPtr != NULL && !check_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id)){
                    ss_log << "dbo_import ("<<  *it << ") and dbo_pefile (" << dbo_pefile->hash_id << ")";    
                    //ss_log << "insert operation.\n";
                    LOG4CXX_TRACE(this->myLogger, ss_log.str());
                    ss_log.str("");
              
                    
                    dbo::ptr<ImportAssociations> dbo_import_assoc = session.add(importAssocPtr);
                    dbo_import_assoc.modify()->import_relation = dbo_import;
                    dbo_import_assoc.modify()->pefile = dbo_pefile; 
                    update_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id);
                }else{
                    ss_log << "[-] import_association ("<< *it << ") was already in the DB.";    
                    LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");
                    continue;                    

                    if (importAssocPtr)
                    {
                        delete importAssocPtr;
                    }
                    else
                    {
                        ss_log << "[-] importAssocPtr was NULL, something bad is happening.";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                    }
                }
            }
            ss_log << "[+] Done creating Import Associations for existing imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");

            ss_log << "[=] Creating Import Associations for new imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");

            std::vector< dbo::ptr<Import> >::iterator it_imports = dbo_import_vector.begin();
            for (; it_imports != dbo_import_vector.end(); it_imports++){
                ImportAssociations *importAssocPtr = new ImportAssociations(*(*it_imports), *pefilePtr);
                if (importAssocPtr != NULL && !check_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id)){
                    //ss_log << "dbo_import ("<< (*(*it_imports)).hash_id << ") and dbo_pefile (" << dbo_pefile->hash_id << ")\n";    
                    //ss_log << "insert operation.\n";
                    //LOG4CXX_WARN(this->myLogger, ss_log.str());
                    ss_log.str("");

                    dbo::ptr<ImportAssociations> dbo_import_assoc = session.add(importAssocPtr);
                    dbo_import_assoc.modify()->import_relation = *it_imports;
                    dbo_import_assoc.modify()->pefile = dbo_pefile; 
                    update_hash_cache(IMPORTS_ASSOC, importAssocPtr->hash_id);
                }else{
                    if (importAssocPtr)
                        delete importAssocPtr;
                    else
                    {
                        ss_log << "[-] importAssocPtr was NULL, something bad is happening.";    
                        LOG4CXX_WARN(this->myLogger, ss_log.str());
                        ss_log.str("");
                    }
                }   
            }
            ss_log << "[+] Done creating Import Associations for new imports.";    
            LOG4CXX_DEBUG(this->myLogger, ss_log.str());
            ss_log.str("");


            ss_log << "Committing imports and associations.";    
            LOG4CXX_TRACE(this->myLogger, ss_log.str());
            ss_log.str("");

            //transaction_add_import_associations.commit();
        ss_log << "[+] Completed processing the imports.";    
        LOG4CXX_DEBUG(this->myLogger, ss_log.str());
        ss_log.str("");
        process_imports_tran.commit();
        this->session.flush();


    }
*/
}