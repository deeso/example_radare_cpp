/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <Wt/Dbo/Dbo>
#include <Wt/WLogger>
#include <string>


#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <boost/filesystem.hpp>

//#include <PeLib.h>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "dbo_orm_postgres.hpp"
#include "dbo_orm_defs_radare.hpp"
// right now need to build the pelib first.
// g++ file_analysis.cpp util.cpp dbo_orm_postgres.cpp dbo_orm_base.cpp -o ../bins/file_analysis -lpq  -lwtdbo -lwt -lwtdbopostgres  -lwtdbosqlite3 -lmagic -lssl -lcrypto -lboost_filesystem -lboost_system -llog4cxx  -L../lib/ -L../contrib/PeLib/source/ -lPeLib -I../include -I../contrib/PeLib/source/


namespace dbo = Wt::Dbo;

namespace file_analysis{

namespace mains{
int main5_test_sectionReading(int argc, char **argv){
   
    log4cxx::LoggerPtr mainLogger = getLogger();

    if (argc < 3){
        std::cerr <<  argv[0] << " <file> <sqlite_db>\n";
        return -1;
    }
    LOG4CXX_INFO (mainLogger, "simply reading the .text section");
    //ORMSqlLite test(argv[2]);
    std::string filename = argv[1];
    
    LOG4CXX_INFO (mainLogger, "opening pe file");

    PeLib::PeFile* pef =  PeLib::openPeFile(filename);
    pef->readMzHeader();
    pef->readPeHeader();


    // find the .text section
    uint64_t sectionNum = -1;
    PeLib::dword ptrData = 0, dataSz = 0;


    PeLib::PeHeaderT<32>& peh32 = static_cast<PeLib::PeFileT<32> *>(pef)->peHeader();

        for (int i=0;i<peh32.getNumberOfSections();i++)
        {
            std::string sectionName = peh32.getSectionName(i);
            long vAddr = peh32.getVirtualAddress(i),
                 vSize = peh32.getVirtualSize(i),
                 rSize = peh32.getSizeOfRawData(i),
                 characteristics = peh32.getCharacteristics();

            float entropy = 0.0;
            std::cout << "Section Name: " << sectionName << std::endl;
            std::cout << "Raw Size: " << rSize << std::endl;
            std::cout << "Section Name: " << vSize << std::endl;

            if (sectionName == std::string(".text")){
                sectionNum = i;
                ptrData = peh32.getPointerToRawData(i);
                dataSz = peh32.getSizeOfRawData(i);
                break;
            }



        }

    if (dataSz == 0)
        return -1;

    char *buffer;
    buffer = new char[dataSz];

    std::ifstream inFile (filename.c_str(), std::ios::in | std::ios::binary);
    inFile.seekg(ptrData);
    inFile.read((char*)buffer, dataSz);

    std::ofstream myFile ("data.bin", std::ios::out | std::ios::binary);
    myFile.write ((const char *)buffer, dataSz);


    LOG4CXX_INFO (mainLogger, "wrote the .text section to data.bin");
    delete buffer;
    delete pef;
    

    LOG4CXX_INFO (mainLogger, "done");
    


}


int main(int argc, char **argv){
   
    log4cxx::LoggerPtr mainLogger = getLogger();

    if (argc < 3){
        std::cerr <<  argv[0] << " <file> <sqlite_db>\n";
        return -1;
    }
    
    ORMSqlLite test(argv[2]);
    std::string filename = argv[1];
    
    LOG4CXX_INFO (mainLogger, "opening pe file");

    PeLib::PeFile* pef =  PeLib::openPeFile(filename);
    pef->readMzHeader();
    pef->readPeHeader();

    LOG4CXX_INFO (mainLogger, "initing the sqlite database");
    test.init_database();

    

    LOG4CXX_INFO (mainLogger, "processing the file");
    pef->visit(test);


}


int main3_test_post_gres_init(int argc, char **argv){
    log4cxx::LoggerPtr mainLogger = getLogger();

    if (argc < 7){
        std::cerr <<  argv[0] << " <filename> <host> <port> <user> <password> <dbname>\n";
        return -1;
    }

    std::string filename = argv[1],
                host = argv[2],
                port = argv[3],
                user = argv[4],
                password = argv[5],
                dbname = argv[6];
                
    ORMPostgres test(host, user, password, port, dbname);
    
    LOG4CXX_INFO (mainLogger, "[=] opening pe file");
    
    PeLib::PeFile* pef =  PeLib::openPeFile(filename);
    pef->readMzHeader();
    pef->readPeHeader();

    
    LOG4CXX_INFO (mainLogger, "[=] processing the file");
    
    pef->visit(test);



}

int main4_test_post_gres_init(int argc, char **argv){
    log4cxx::LoggerPtr mainLogger = getLogger();

    if (argc < 7){
        std::cerr <<  argv[0] << " <directory> <host> <port> <user> <password> <dbname>\n";
        return -1;
    }

    std::string filename = argv[1],
                host = argv[2],
                port = argv[3],
                user = argv[4],
                password = argv[5],
                dbname = argv[6];

                
    ORMPostgres test(host, user, password, port, dbname);
    std::string scan_path = argv[1];
    
    LOG4CXX_INFO (mainLogger, "[=] processing the directory with results going to postgres");
    try{
        test.scan_and_process_directory_of_exes(scan_path);    
    }catch(...){
        LOG4CXX_WARN(mainLogger, "[-] Failure processing the directory");
        
    }
    LOG4CXX_INFO(mainLogger, "[+] Completed processing the directory");


}


int main2(int argc, char **argv){
    log4cxx::LoggerPtr mainLogger = getLogger();

    if (argc < 3){
        std::cerr <<  argv[0] << " <directory> <sqlite_db>\n";
        return -1;
    }
    std::string scan_path = argv[1];
    
    LOG4CXX_INFO (mainLogger, std::string("[=] Scanning path: ") +scan_path);

    LOG4CXX_INFO (mainLogger, std::string("[=] Saving results to db: ") + std::string(argv[2]));
    
    ORMSqlLite test(argv[2]);
    
    LOG4CXX_INFO (mainLogger, "[=] processing the directory with results going to sqlite");
    try{
        test.scan_and_process_directory_of_exes(scan_path);    
    }catch(...){
        LOG4CXX_WARN(mainLogger, "[-] Failure processing the directory");
        
    }
    LOG4CXX_INFO(mainLogger, "[+] Completed processing the directory");
    

}
}


}

int main(int argc, char **argv){
    //file_analysis::mains::main(argc, argv);
    //file_analysis::mains::main2(argc, argv);
    file_analysis::mains::main4_test_post_gres_init(argc, argv); 
    //file_analysis::mains::main5_test_sectionReading(argc, argv);
}
