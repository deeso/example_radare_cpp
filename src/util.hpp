/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
  
#include <fstream>
#include <string>

#ifndef UTIL_H
#define UTIL_H

#include <r_hash.h>
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>


const  std::string LOGGER_CONF = "../log/log4cxx.xml";

void init_logger();
void init_logger(std::string logger_conf);

log4cxx::LoggerPtr getLogger(std::string loggerName);
log4cxx::LoggerPtr getLogger();


bool is_exe_or_dll(std::string & path);
uint64_t get_file_data(std::string & path, std::string & file_data);
std::string calc_sha256 (std::string &file_data);
std::string calc_md5 (std::string & file_data);
void scan_directory_for_exes( std::string & start_dir, std::ofstream & output_file);
//void scan_directory( std::string start_dir);
std::vector<std::string> *scan_directory( std::string start_dir);

uint64_t hashSymbols(const std::string &one, const std::string &two);
uint64_t hashSymbols(const std::string &one);

uint64_t long_hash_from_sha256_ascii(const std::string & sha256HashBuffer_string);
uint64_t long_hash_from_sha256(const unsigned char *sha256HashBuffer);

void countBytes(char *buffer, uint64_t sz, 
                boost::container::map<char, uint64_t> & m_ch_cnt);

std::string getPrintableString(std::string & in);
std::string hexlify(std::string & in);



#endif