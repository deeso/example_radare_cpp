/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <Wt/Dbo/Dbo>
#include <Wt/WLogger>
#include <string>


#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>
#include <exception>

#include <cstring>

#include <algorithm>
#include <string>
#include <boost/filesystem.hpp>

#include <postgresql/libpq-fe.h>
#include <vector>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "util.hpp"
#include "dbo_orm_postgres.hpp"
#include "dbo_orm_defs_radare.hpp"


namespace dbo = Wt::Dbo;

namespace file_analysis{

    std::string ORMPostgres::getConnectionString(){        
        std::stringstream ss;
        ss << "host=" << this->host << " ";
        ss << "user=" << this->user << " ";
        if (this->password != "")
            ss << "password=" << this->password << " ";
        ss << "port=" << this->port << " ";
        ss << "dbname=" << this->dbname << " ";
        std::string connect_string = ss.str();
        return connect_string;
    }
    bool ORMPostgres::createDatabase(){
        
        std::stringstream ss;
        ss << "host=" << this->host << " ";
        ss << "user=" <<  this->user << " ";
        ss << "password=" <<  this->password << " ";
        ss << "port=" <<  this->port << " ";
        ss << "dbname=" << "template1" << " ";

        std::stringstream ss_log;
        ss_log << "[=] Using the following parameters:\n";
        ss_log  << "\t[*] host=" << this->host << "\n";
        ss_log  << "\t[*] user=" <<  this->user << "\n";
        ss_log  << "\t[*] port=" <<  this->port << "\n";
        LOG4CXX_INFO (this->myLogger, ss_log.str());
        ss_log.str("");

        PGconn *psql;
        psql = PQconnectdb((ss.str()).c_str());
        if (PQstatus(psql) != CONNECTION_OK){
            ss_log << "[-] Unable to connect to the SQL Server." <<this->host <<":"<< this->port;
            LOG4CXX_WARN (this->myLogger, ss_log.str());            
            PQfinish(psql);
            return false;
        }

        ss_log << "[+] Connected to the SQL Server." <<this->host <<":"<< this->port;
        LOG4CXX_INFO (this->myLogger, ss_log.str());
        ss_log.str("");        

        
        std::stringstream create_db;
        create_db << "create database " <<  this->dbname << ";" ;
        std::string create_db_str = create_db.str();
        

        LOG4CXX_INFO (this->myLogger, std::string( "[=] Executing: ") + create_db_str);
        PGresult *res = PQexec(psql, create_db_str.c_str());
        
        bool result = false;
        if (PQresultStatus(res) == PGRES_COMMAND_OK)
        {
            result = true;        
        }
        PQclear(res);
        PQfinish(psql);
        
        if (result)
        {
            LOG4CXX_INFO (this->myLogger, std::string("[+] Created db: ") + this->dbname);
        }else
        {
            LOG4CXX_WARN (this->myLogger, std::string( "[-] Failed command to create the db: ") + this->dbname);                        
        }
        return result;
    }

    void ORMPostgres::init_dbconnection(){
        try{
            std::cout << "[=] Connecting and initializing the db.\n" << std::endl;
            this->postgres = new Wt::Dbo::backend::Postgres(this->getConnectionString());
            if (this->postgres == NULL){
                LOG4CXX_WARN (this->myLogger, std::string("[-] Failed to connect to the db." ));
                throw ORMException("Failed to connect to the DB.\n");                
            }   

        }catch (std::exception& e){
            std::cout << "[=] trying to connect to the db again, got:\n" << e.what()<< std::endl;
            
            if (!createDatabase()){
                LOG4CXX_WARN (this->myLogger, std::string("[-] Unable connect to the db again." ));
                this->postgres = NULL;
                throw ORMException("Failed to connect to the DB.\n");
            }
            this->postgres = new Wt::Dbo::backend::Postgres(this->getConnectionString());
        }        
        this->session.setConnection(*this->postgres);
        LOG4CXX_INFO (this->myLogger, std::string("[+] DB Connection Setup complete." ));
        this->init_database();
        LOG4CXX_INFO (this->myLogger, std::string("[+] DB Init complete." ));

    }
    ORMPostgres::ORMPostgres(const std::string &host, 
                const std::string &user,
                const std::string &password,
                const std::string &port,
                const std::string & dbname): ORMBase(dbname), 
                                             host(host), user(user), 
                                             port(port), password(password)
    {        
        this->init_dbconnection();
    }

    ORMPostgres::ORMPostgres(const std::string & dbname): ORMBase(dbname), 
                                             host("127.0.0.1"), user("postgres"), 
                                             port("5432"), password("")
    {
        this->init_dbconnection();
    } 
}