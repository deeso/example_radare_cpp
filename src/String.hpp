/*
    
   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include <string>


class String{


   long mStringHashValue;
   long mLocation;
   long mRelativeVirtualAddress;
   long mOrdinal;
   std::string mString;
   std::std::vector<long> mStringAssociation;

public:
   String(): long mStringHashValue(0), mLocation(0), 
         mRelativeVirtualAddress(0), mString(""){}
    
   String(RBinString *rBinString): long mStringHashValue(0), mLocation(0), 
         mRelativeVirtualAddress(0), mString(""){
        
        mString = rBinString->string;
        mRelativeVirtualAddress = rBinString->rva;
        mOffset = rBinString->offset;
        mSize = rBinString->size;
        mOrdinal = rBinString->ordinal;
        mStringHashValue = hashSymbols(String);
   }
   String(const String &oString): long mStringHashValue(0), mLocation(0), 
         mRelativeVirtualAddress(0), mString(""){
        
        mString = oString.getString();
        mRelativeVirtualAddress = oString.getRelativeVirtualAddress();
        mOffset = oString.getOffset();
        mSize = oString.getSize();
        mOrdinal = oString.getOrdinal();
        mStringHashValue = oString.getHashSymbols(String);
   }
   
};