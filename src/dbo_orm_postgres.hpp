/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Postgres>

#include <string>

#include "dbo_orm_base.hpp"

#ifndef DBO_ORM_POSTGES_H
#define DBO_ORM_POSTGES_H

namespace dbo = Wt::Dbo;

namespace file_analysis{
 

class ORMPostgres: public ORMBase{

    std::string host, user, password, port;
    std::string getConnectionString();
    bool createDatabase();

public:
    Wt::Dbo::backend::Postgres *postgres;

    void init_dbconnection();

    ORMPostgres(const std::string &host, 
                const std::string &user,
                const std::string &password,
                const std::string &port,
                const std::string & dbname);

    ORMPostgres(const std::string & dbname);
    
    ~ORMPostgres(){
        delete this->postgres;
    }


};



}
#endif