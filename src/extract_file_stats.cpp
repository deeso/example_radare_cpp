#include <Poco/Process.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <stdlib.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "tspriorityqueue.hpp"
#include "util.hpp"
#include "Processor.hpp"
#include "PocoProcessor.hpp"

log4cxx::LoggerPtr mainLogger;

std::string QuickTest = std::string("VirusShare_4eba9312b6f81e00f6c5800cd4f808d3,,/research_data/radare_error_cases/VirusShare_4eba9312b6f81e00f6c5800cd4f808d3,EXEC (Executable file),PE32,pe,i386,windows,Windows GUI,NONE,20,0,1,52d0c,400000,69a000,683000,b8bebc83cc1c8f5a,626d84561ff72e24bdfb3713d098a68c5a8f1ccc83bcbeb891db90b782d34bfb,4eba9312b6f81e00f6c5800cd4f808d3,,x86,6.81363,0.851704,4c2fff2f");


std::string * process_with_radare(std::string &filename){
    std::stringstream ss_out;
    
    LOG4CXX_TRACE (mainLogger, "[=] processing the file with results going to stdout");
    try{
        UniqueTSPQueue<std::string> filenames_queue;
        filenames_queue.push(filename);

        Processing::Processor processor(1, filenames_queue);
        std::string started_filename = processor.startOneWorker();
        
        
        LOG4CXX_TRACE (mainLogger, (std::string("[=] Started the processing on: ") +(started_filename)));


        LOG4CXX_TRACE (mainLogger, std::string("[+] Completed last file in the queue, waiting for the remaining threads to  finish."));
        

        

        while (processor.getCurrentProcessorThreadsCnt() > 0){
            ss_out.str("");
            ss_out << "Waiting on " << processor.getCurrentProcessorThreadsCnt() << " to complete.";
            LOG4CXX_TRACE (mainLogger, ss_out.str());        
            LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 100ms, waiting for the remaining threads to  finish."));
            boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
            
        }


        long fileId = processor.getFirstCompletedFileId();
        const File * f = processor.getCompletedFileById(fileId);
        std::cout << f->summarizeCSV() << std::endl;
        if(f == NULL)
            return NULL;

        std::string * output = new std::string(f->summarizeCSV());
                    
        LOG4CXX_TRACE(mainLogger, "[+] Completed processing the directory");
        return output;
    }catch(...){
        LOG4CXX_WARN(mainLogger, "[-] Failure processing the directory");
        
    }
    
    return NULL;
}

std::string * run_quick_test(std::string & wtf){
    std::string *output = process_with_radare(wtf);
    
    File *f = new File();
    f->fromCSV(QuickTest);
    std::string result = f->summarizeCSV();
    delete f;
    if (output && *output == result){
        LOG4CXX_TRACE (mainLogger, std::string("[+] Matches quick check."));
    }
    return output;


}

int main(int argc, char **argv){

    mainLogger = getLogger();
    if (argc < 2){
        std::cerr <<  argv[0] << " <filename>\n";
        return -1;
    }
    std::string wtf(argv[1]);
    std::string *output;
    output = process_with_radare(wtf);
    //output = run_quick_test(wtf);
    if (output){
        std::cout << *output << std::endl;
        delete output;
    }

}
