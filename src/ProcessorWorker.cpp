
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip> 
#include <string>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include "File.hpp"
#include "Processor.hpp"
#include "ProcessorWorker.hpp"

namespace Processing{
    void ProcessorWorker::setTaskId(long task_id){ task_id_ = task_id; }
    long ProcessorWorker::getTaskId(){return task_id_;}
    void ProcessorWorker::start(std::string &filename){
        mFile_ = NULL;
        m_Thread = new boost::thread(&ProcessorWorker::processFile, this, filename); 
    }
    void ProcessorWorker::join(){
        m_Thread->join();
    }

    void ProcessorWorker::releaseResultOwnerShip(){
        ownResult_ = false;
    }


    void ProcessorWorker::processFile(std::string &filename)
    {
        
        LOG4CXX_TRACE(this->myLogger, (std::string("Worker processing: ") + filename));
        
        mFile_ = new File(filename);
        taskCompleted = true;
        {
            std::stringstream ss_out;
            ss_out << "Thread " << task_id_ << ": done processing file";    
            LOG4CXX_TRACE(this->myLogger, ss_out.str());
        }
        LOG4CXX_TRACE(this->myLogger, std::string("Calling the handler for completed files."));
        processor_.handleFileParsingCompletion(*this);
    }
    
    File *ProcessorWorker::getFile(){
        return mFile_; 
    }
    bool ProcessorWorker::isAlive(){
        if (m_Thread == NULL)
            return false;
        
        boost::posix_time::time_duration delay(0,0,0,0); 
        return m_Thread->timed_join(delay); 
    }
}

