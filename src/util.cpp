/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <boost/filesystem.hpp>
#include <boost/container/map.hpp>
#include <math.h>


#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>
#include <cctype> 

#include <openssl/sha.h>
#include <openssl/md5.h>

#include <magic.h>
#include <cstring>

#include <algorithm>
#include <string>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>
#include <log4cxx/xml/domconfigurator.h>

#include <r_hash.h>
#include "util.hpp"



// taken from: http://stackoverflow.com/questions/9620891/handling-of-conversions-from-and-to-hex
int to_int(int c) {
  if (not isxdigit(c)) return -1; // error: non-hexadecimal digit found
  if (isdigit(c)) return c - '0';
  if (isupper(c)) c = tolower(c);
  return c - 'a' + 10;
}

template<class InputIterator, class OutputIterator> 
int unhexlify(InputIterator first, InputIterator last, OutputIterator ascii) {
  while (first != last) {
    int top = to_int(*first++);
    int bot = to_int(*first++);
    if (top == -1 or bot == -1)
      return -1; // error
    *ascii++ = (top << 4) + bot;
  }
  return 0;
}

std::string hexlify(std::string & in){
    std::stringstream ss;
    ss << std::hex << std::setfill('0');

    std::string::iterator it = in.begin();
    for (; it != in.end(); it++)
    {
        
        ss << std::setw(2) << static_cast<unsigned>(*it);
    }

    std::string output = ss.str();
    return output;

}


bool LOGGER_INITTED = false;
log4cxx::LoggerPtr QFSCANNER_LOGGER;
bool QFSCANNER_INITTED = false;
std::string DEFAULT_LOGGER = "file_analysis";

void init_logger(){
    init_logger(LOGGER_CONF);
}


void init_logger(std::string logger_conf){
    log4cxx::xml::DOMConfigurator::configure(logger_conf.c_str());
    QFSCANNER_LOGGER = log4cxx::Logger::getLogger(DEFAULT_LOGGER);
    QFSCANNER_INITTED = true;
}

log4cxx::LoggerPtr getLogger(std::string name){
    // TODO add the name to a map, if the logger has not yet
    // been initialized, otherwise return the mapped logger
    if (name == DEFAULT_LOGGER)
    {
        if (!QFSCANNER_INITTED)
        {
            init_logger();

        }
        return QFSCANNER_LOGGER;

    }
    return log4cxx::Logger::getLogger(name);
}

log4cxx::LoggerPtr getLogger(){
    return getLogger(DEFAULT_LOGGER);
}


uint64_t long_hash_from_sha256_ascii(const std::string & sha256_hash_string){
    unsigned char hash[SHA256_BLOCK_LENGTH];
    unhexlify(sha256_hash_string.begin(), sha256_hash_string.end(), hash);
    return long_hash_from_sha256(hash);
}

uint64_t long_hash_from_sha256(const unsigned char *sha256HashBuffer){
    uint64_t first8, second8, third8;
    first8 = second8 = third8 = 0;

    //first8 =  *(uint64_t *)sha256HashBuffer;
    //second8 = *(uint64_t *)(sha256HashBuffer+8);
    third8 =  *(uint64_t *)(sha256HashBuffer+16);
    return first8 ^ second8 ^ third8;
}

// taken from http://stackoverflow.com/questions/8820399/c-sharp-4-0-how-to-get-64-bit-hash-code-of-given-string
uint64_t hashSymbols(const std::string &one, const std::string &two){
    std::string str = one+two;
    RHash *ctx = r_hash_new(1, 0);
    ut8 *hash = r_hash_do_sha256(ctx, (ut8 *) str.c_str(), str.size());
    long xored_hash = long_hash_from_sha256((const unsigned char *)hash);
    r_hash_free(ctx);
    return xored_hash;
} 

uint64_t hashSymbols(const std::string &one){
    std::string str = one;
    RHash *ctx = r_hash_new(1, 0);
    ut8 *hash = r_hash_do_sha256(ctx, (ut8 *) str.c_str(), str.size());
    long xored_hash = long_hash_from_sha256((const unsigned char *)hash);
    r_hash_free(ctx);
    return xored_hash;

}

bool is_exe_or_dll(std::string & path){
    std::string magic_str = "";
    const char *myt_result;

    magic_t myt = magic_open(MAGIC_CONTINUE|MAGIC_ERROR|MAGIC_MIME);
    magic_load(myt,NULL);
    myt_result =  magic_file(myt, path.c_str());
    
    if (myt_result == NULL){
        magic_close(myt);    
        return false;
    }

    magic_str = std::string( myt_result, strlen(myt_result));
    std::transform(magic_str.begin(), magic_str.end(), magic_str.begin(), ::tolower);
    magic_close(myt);

    if (magic_str.find("pe") == 0  ||
        magic_str.find("exe") != std::string::npos ||
        magic_str.find("ocx") != std::string::npos ||
        magic_str.find("sfx") != std::string::npos ||
        magic_str.find("ms-dos") != std::string::npos ||
        magic_str.find("self extracting") != std::string::npos ||
        magic_str.find("self-extracting") != std::string::npos){
        return true;
    }
    return false;
}

uint64_t get_file_data(std::string & path, std::string & file_data){
    std::ifstream is(path.c_str(), std::ifstream::binary);
    if (is){
        // pass
    }else{

        std::cerr << "Unable to open file: " << path;
        return 0;
    }
    is.seekg(0, std::ios::end);
    uint64_t sz = is.tellg();   
    file_data.resize(sz);
    is.seekg(0, std::ios::beg);
    is.read(&file_data[0], file_data.size());
    is.close();
    return sz;
}

std::string calc_sha256 (std::string & data)
{
    
    std::string str = data;
    RHash *ctx = r_hash_new(1, 0);
    ut8 *hash = r_hash_do_sha256(ctx, (ut8 *) str.c_str(), str.size());

    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (int i = 0; i < R_HASH_SIZE_SHA256; i++)
    {
        ss << std::setw(2) << static_cast<unsigned>(hash[i]);
    }
    r_hash_free(ctx);
    std::string output = ss.str();
    return output;
}  


std::string calc_md5 (std::string & data)
{
    
    std::string str = data;
    RHash *ctx = r_hash_new(1, 0);
    ut8 *hash = r_hash_do_md5(ctx, (ut8 *) str.c_str(), str.size());

    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (int i = 0; i < R_HASH_SIZE_MD5; i++)
    {
        ss << std::setw(2) << static_cast<unsigned>(hash[i]);
    }

    r_hash_free(ctx);;
    std::string output = ss.str();
    return output;

}

void scan_directory_for_exes( std::string & start_dir, std::ofstream & output_file){
    
    boost::filesystem::path top_dir(start_dir);
    unsigned long files_processed = 0;
    
    std::string sha256_hash = "";
    std::string md5_hash = "";

    output_file << "PATH" << "\t" << "SHA256" << "\t"<< "MD5" << "\t" << "PEFILE" << "\t" << "SIZE" <<   std::endl;
    for ( boost::filesystem::recursive_directory_iterator end, dir(top_dir); 
        dir != end; ++dir, ++files_processed) {



        std::string the_path = (*dir).path().native();
        if (the_path.find("/Microsoft/CryptnetUrlCache/Content/") != std::string::npos){
            std::cout << "Skipping " << the_path <<".  Might want to check it out." << std::endl;
        }

        if (boost::filesystem::is_directory(the_path))
            continue;

        bool exe_o_dll = is_exe_or_dll(the_path);
        if (!exe_o_dll){
            continue;
        }
        
        std::string file_data = "";
        uint64_t sz = get_file_data(the_path, file_data);
        sha256_hash = calc_sha256(the_path );
        md5_hash = calc_md5(the_path);
        output_file << the_path << "\t" << sha256_hash << "\t"<< md5_hash << "\t" << exe_o_dll << "\t" << sz <<   std::endl;


    }
}
/*
void scan_directory( std::string start_dir){
    
    boost::filesystem::path top_dir(start_dir);
    unsigned long files_processed = 0;
    
    std::string sha256_hash = "";
    std::string md5_hash = "";

    for ( boost::filesystem::recursive_directory_iterator end, dir(top_dir); 
        dir != end; ++dir, ++files_processed) {


        std::string the_path = (*dir).path().native();
        if (boost::filesystem::is_directory(the_path))
            continue;

        std::string file_data = "";
        uint64_t sz = get_file_data(the_path, file_data);
        sha256_hash = calc_sha256(the_path );
        md5_hash = calc_md5(the_path);
        std::cout << the_path << "\t" << sha256_hash << "\t"<< md5_hash << "\t" << sz <<   std::endl;


    }
}
*/
std::vector<std::string> *scan_directory( std::string start_dir){
    
    std::vector<std::string> *ptrVString = new std::vector<std::string>();
    boost::filesystem::path top_dir(start_dir);
    unsigned long files_processed = 0;
    
    
    for ( boost::filesystem::recursive_directory_iterator end, dir(top_dir); 
        dir != end; ++dir, ++files_processed) {


        std::string the_path = (*dir).path().native();
        if (boost::filesystem::is_directory(the_path))
            continue;

        ptrVString->push_back(the_path);
    }
    return ptrVString;
}



void countBytes(char *buffer, uint64_t sz, 
                boost::container::map<char, uint64_t> & m_ch_cnt){

    for(uint64_t i = 0; i < sz; i++){
        char c = *(buffer + i);
        if (m_ch_cnt.find(c) == m_ch_cnt.end()){
            m_ch_cnt[c] = 0;
        }
        m_ch_cnt[c] += 1;
    }
}

std::string getPrintableString(std::string & in){
    std::stringstream ss;

    std::string::iterator it = in.begin();

    for (; it != in.end(); it++){
        if (std::isprint(*it))
            ss << *it;
        else
            ss << "-";
        if (*it == '\0'){
            break;
        }
    }
    std::string out = ss.str();
    return out;
}
