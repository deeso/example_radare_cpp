#include <Poco/Process.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <stdlib.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "tspriorityqueue.hpp"
#include "util.hpp"
#include "Processor.hpp"
#include "PocoProcessor.hpp"

log4cxx::LoggerPtr mainLogger;

void dump_completed_files(bool * print_header, Processing::Processor & processor, std::ofstream & outfile){

    std::vector<long> * fileIds = processor.getCompletedFileIds();
    std::vector<long>::iterator fileIds_iter = fileIds->begin();        
    int dumped_data = 0;

    if (fileIds->size() == 0){
        delete fileIds;
        return;
    }

    for(;fileIds_iter != fileIds->end(); fileIds_iter++){
            
        const File * f = processor.getCompletedFileById(*fileIds_iter);
        if(f == NULL)
            continue;

        if(!(*print_header)){
            outfile <<  f->summarizeCSVHeader() << std::endl;
            *print_header = true;
        }

        outfile << f->summarizeCSV() << std::endl;
        dumped_data += 1;
    }
    std::stringstream ss_out;
    ss_out << "[+] Dumped [" << dumped_data << "] results to file.";
    LOG4CXX_DEBUG (mainLogger, ss_out.str());

    ss_out.str("Releasing use of the file objects");
    LOG4CXX_DEBUG (mainLogger, ss_out.str());
    processor.releaseAccessToCompleteFilesById(fileIds);
    ss_out.str("Deleting the dumped file objects");
    LOG4CXX_DEBUG (mainLogger, ss_out.str());
    processor.deleteCompletedFilesById(fileIds);
    delete fileIds;

}

int use_rabin2(long threads, std::string &scan_path, std::string &csv_filename){
    std::stringstream ss_out;
    
    LOG4CXX_TRACE (mainLogger, std::string("[=] Scanning path: ") +scan_path);

    LOG4CXX_TRACE (mainLogger, std::string("[=] Saving results to file: ") + csv_filename);
    
    std::ofstream outfile;
    outfile.open (csv_filename.c_str(), std::ios::app);
    //bool printed_header = false;
        

    
    LOG4CXX_TRACE (mainLogger, "[=] processing the directory with results going to csv file");
    try{
        std::vector<std::string> *ptrVString = scan_directory(scan_path);
        UniqueTSPQueue<std::string> filenames_queue;
        

        for (std::vector<std::string>::iterator it = ptrVString->begin() ; it != ptrVString->end(); ++it){
            filenames_queue.push(*it);
        }
        LOG4CXX_TRACE (mainLogger, std::string("[+] Scanning complete, processing the files."));
        delete ptrVString;

        Processing::PocoProcessor processor(threads, filenames_queue);


        while(processor.getRemainingFilenameCnt() > 0){
            std::vector<std::string> * started_filenames = processor.startMultipleWorkers();
            for (std::vector<std::string>::iterator it2 = started_filenames->begin() ; it2 != started_filenames->end(); ++it2){
                LOG4CXX_TRACE (mainLogger, (std::string("[=] Started the processing on: ") +(*it2)));
        
            }        
            delete started_filenames;

            LOG4CXX_TRACE (mainLogger, std::string("[=] Dumping results to file."));
            //dump_completed_files(&printed_header, processor, outfile);

            if (processor.getRemainingFilenameCnt() > 0){

                ss_out.str("");
                ss_out << "Waiting to process the remaining ["; 
                ss_out << processor.getRemainingFilenameCnt();
                ss_out << "] files.";
                LOG4CXX_TRACE (mainLogger, ss_out.str());

                ss_out.str("");
                ss_out << "Waiting on " << processor.getCurrentProcessorThreadsCnt() << " to complete.";
                //processor.waitThreadToComplete();
            }
                
            while (processor.getCurrentProcessorThreadsCnt() > threads){
                LOG4CXX_TRACE (mainLogger, ss_out.str());        
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 100ms, waiting for the remaining threads to  finish."));

                LOG4CXX_TRACE (mainLogger, ss_out.str());
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));
                // the below code is not really safe, a race condition will occur if
                // thread dies, but does not have a chance to finish its cleanup.
                //processor.cleanupDeadThreads();
            }

        }
        LOG4CXX_TRACE (mainLogger, std::string("[+] Completed last file in the queue, waiting for the remaining threads to  finish."));
        

        

        while (processor.getCurrentProcessorThreadsCnt() > 0){
            ss_out.str("");
            ss_out << "Waiting on " << processor.getCurrentProcessorThreadsCnt() << " to complete.";
            LOG4CXX_TRACE (mainLogger, ss_out.str());        
            // the below code is not really safe, a race condition will occur if
            // thread dies, but does not have a chance to finish its cleanup.
            //processor.cleanupDeadThreads();
            
            if (processor.getCurrentProcessorThreadsCnt() > 10){
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 1000ms, waiting for the remaining threads to  finish."));
                boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
            }else{
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 100ms, waiting for the remaining threads to  finish."));
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));                
            }
            
        }

        LOG4CXX_TRACE (mainLogger, std::string("[+] Writing results out to disk."));
        //dump_completed_files(&printed_header, processor, outfile);

        //processor.summarizeAllFiles();
        //boost::container::map<long, File *> & completed_files = processor.getProcessedFiles();
        //boost::container::map<long, File*>::iterator iterFileMap = completed_files.begin();
        
        
        outfile.close();

        LOG4CXX_TRACE (mainLogger, std::string("[+] Results written out to disk."));
    }catch(...){
        LOG4CXX_WARN(mainLogger, "[-] Failure processing the directory");
        
    }
    LOG4CXX_TRACE(mainLogger, "[+] Completed processing the directory");
    return 0;
}

int use_radare_bindings(long threads, std::string &scan_path, std::string &csv_filename){
    std::stringstream ss_out;
    
    LOG4CXX_TRACE (mainLogger, std::string("[=] Scanning path: ") +scan_path);

    LOG4CXX_TRACE (mainLogger, std::string("[=] Saving results to file: ") + csv_filename);
    
    std::ofstream outfile;
    outfile.open (csv_filename.c_str(), std::ios::app);
    bool printed_header = false;
        

    
    LOG4CXX_TRACE (mainLogger, "[=] processing the directory with results going to csv file");
    try{
        std::vector<std::string> *ptrVString = scan_directory(scan_path);
        UniqueTSPQueue<std::string> filenames_queue;
        

        for (std::vector<std::string>::iterator it = ptrVString->begin() ; it != ptrVString->end(); ++it){
            filenames_queue.push(*it);
        }
        LOG4CXX_TRACE (mainLogger, std::string("[+] Scanning complete, processing the files."));
        delete ptrVString;

        Processing::Processor processor(threads, filenames_queue);


        while(processor.getRemainingFilenameCnt() > 0){
            std::vector<std::string> * started_filenames = processor.startMultipleWorkers();
            for (std::vector<std::string>::iterator it2 = started_filenames->begin() ; it2 != started_filenames->end(); ++it2){
                LOG4CXX_TRACE (mainLogger, (std::string("[=] Started the processing on: ") +(*it2)));
        
            }        
            delete started_filenames;

            LOG4CXX_TRACE (mainLogger, std::string("[=] Dumping results to file."));
            dump_completed_files(&printed_header, processor, outfile);

            if (processor.getRemainingFilenameCnt() > 0){

                ss_out.str("");
                ss_out << "Waiting to process the remaining ["; 
                ss_out << processor.getRemainingFilenameCnt();
                ss_out << "] files.";
                LOG4CXX_TRACE (mainLogger, ss_out.str());

                ss_out.str("");
                ss_out << "Waiting on " << processor.getCurrentProcessorThreadsCnt() << " to complete.";
                //processor.waitThreadToComplete();
            }
                
            while (processor.getCurrentProcessorThreadsCnt() > threads){
                LOG4CXX_TRACE (mainLogger, ss_out.str());        
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 100ms, waiting for the remaining threads to  finish."));

                LOG4CXX_TRACE (mainLogger, ss_out.str());
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));
                // the below code is not really safe, a race condition will occur if
                // thread dies, but does not have a chance to finish its cleanup.
                //processor.cleanupDeadThreads();
            }

        }
        LOG4CXX_TRACE (mainLogger, std::string("[+] Completed last file in the queue, waiting for the remaining threads to  finish."));
        

        

        while (processor.getCurrentProcessorThreadsCnt() > 0){
            ss_out.str("");
            ss_out << "Waiting on " << processor.getCurrentProcessorThreadsCnt() << " to complete.";
            LOG4CXX_TRACE (mainLogger, ss_out.str());        
            // the below code is not really safe, a race condition will occur if
            // thread dies, but does not have a chance to finish its cleanup.
            //processor.cleanupDeadThreads();
            
            if (processor.getCurrentProcessorThreadsCnt() > 10){
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 1000ms, waiting for the remaining threads to  finish."));
                boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
            }else{
                LOG4CXX_TRACE (mainLogger, std::string("[=] Sleeping 100ms, waiting for the remaining threads to  finish."));
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));                
            }
            
        }

        LOG4CXX_TRACE (mainLogger, std::string("[+] Writing results out to disk."));
        dump_completed_files(&printed_header, processor, outfile);

        //processor.summarizeAllFiles();
        //boost::container::map<long, File *> & completed_files = processor.getProcessedFiles();
        //boost::container::map<long, File*>::iterator iterFileMap = completed_files.begin();
        
        
        outfile.close();

        LOG4CXX_TRACE (mainLogger, std::string("[+] Results written out to disk."));
    }catch(...){
        LOG4CXX_WARN(mainLogger, "[-] Failure processing the directory");
        
    }
    LOG4CXX_TRACE(mainLogger, "[+] Completed processing the directory");
    return 0;
}


int perform_multiple_file_processing(int argc, char **argv){

    if (argc < 3){
        std::cerr <<  argv[0] << " [--rabin2] <num_threads> <directory> <output_csv_file>\n";
        return -1;
    }

    std::string rabin_check(argv[1]);
    std::string my_rabin("--rabin");

    if (rabin_check.find(my_rabin) != std::string::npos){
        LOG4CXX_TRACE (mainLogger, std::string("[=] Using rabin2."));
        std::string scan_path = argv[3], 
                csv_filename = argv[4];

        long threads = atoi(argv[2]);
    
        return use_rabin2(threads, scan_path, csv_filename );
    }else {
        LOG4CXX_TRACE (mainLogger, std::string("[=] Using radare bindings."));
        std::string scan_path = argv[2], 
                csv_filename = argv[3];

        long threads = atoi(argv[1]);
    
        return use_radare_bindings(threads, scan_path, csv_filename );
    }


}

int main(int argc, char **argv){

    mainLogger = getLogger();

    perform_multiple_file_processing(argc, argv);
}
