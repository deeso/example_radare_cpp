/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
#include <Wt/Dbo/Dbo>
#include <string>

#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/backend/Postgres>


#include <boost/filesystem.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>


#include <openssl/sha.h>
#include <openssl/md5.h>

#include <magic.h>
#include <cstring>

#include <algorithm>
#include <string>

#include <buffer/InputBuffer.h>

#include <boost/container/set.hpp>
#include <exception>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include "dbo_orm_defs_radare.hpp"


#include <stdlib.h>     
#include <time.h>

#ifndef DBO_ORM_BASE_H
#define DBO_ORM_BASE_H

namespace dbo = Wt::Dbo;

namespace file_analysis{

enum HASH_CACHE_TYPE { IMPORTS, SYMBOLS, SECTIONS, STRINGS, METAS, FILES, 
                        IMPORTS_ASSOC, SYMBOLS_ASSOC, SECTIONS_ASSOC,
                        STRINGS_ASSOC, METAS_ASSOC };



extern boost::container::set<long> IMPORT_HASHES;
extern boost::container::set<long> SYMBOL_HASHES;
extern boost::container::set<long> FILES_HASHES;
extern boost::container::set<long> SECTIONS_HASHES;
extern boost::container::set<long> STRINGS_HASHES;
extern boost::container::set<long> METAS_HASHES;

extern boost::container::set<long> SYMBOLS_ASSOC_HASHES;
extern boost::container::set<long> IMPORTS_ASSOC_HASHES;
extern boost::container::set<long> SECTIONS_ASSOC_HASHES;
extern boost::container::set<long> STRINGS_ASSOC_HASHES;
extern boost::container::set<long> METAS_ASSOC_HASHES;


extern volatile bool SRAND_CALLED;
 
class ORMException : public std::exception
{
    std::string s;
public:
    ORMException(std::string ss) : s(ss) {}
    const char* what() const throw() 
    { 
        return s.c_str(); 
    }
    ~ORMException () throw() {};// throw {}
};


class ORMBase{
    
    std::string badpefile_log;


    void init_hash_cache(  HASH_CACHE_TYPE x);
    bool check_hash_cache(  HASH_CACHE_TYPE x, long hash);
    void update_hash_cache(  HASH_CACHE_TYPE x, long hash);
    
    uint64_t processed;

    void process_file(RCore *rCore);    
    void process_sections(RCore *rCore, dbo::ptr<File> & file);
    void process_imports(RCore *rCore, dbo::ptr<File> & file);
    void process_symbols(RCore *rCore, dbo::ptr<File> dbo_file);
    void process_strings(RCore *rCore, dbo::ptr<File> dbo_file);
    
    virtual void init_dbconnection() = 0;

protected:
    log4cxx::LoggerPtr myLogger;


public:
    Wt::Dbo::backend::Postgres *postgres;// = new Wt::Dbo::backend::Postgres(connect_string);

    dbo::Session session;
    std::string dbname;




    ORMBase(const std::string & dbname) : dbname(dbname), processed(0)
    {
        if(!SRAND_CALLED){
            srand(time(NULL));
            SRAND_CALLED = true;
        }
        this->myLogger = getLogger(std::string("ORMBase"));
    }   
    
    void scan_and_process_directory_of_exes( std::string & start_dir);
    void init_database();
    
    ~ORMBase(){}


};



}
#endif
