/*
    
   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <Wt/Dbo/Dbo>
#include <string>

#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Dbo/backend/Postgres>
#include <Wt/WString>

#include <boost/filesystem.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <iomanip>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <openssl/sha.h>
#include <openssl/md5.h>

#include <magic.h>
#include <cstring>

#include <algorithm>
#include <string>

#include "util.hpp"
#include <r_bin.h>

#ifndef DBO_ORM_DEFS_H
#define DBO_ORM_DEFS_H

namespace dbo = Wt::Dbo;


namespace file_analysis{

class Import;
class Symbol;
class File;
class String;
class Section;


class SectionAssociations;
class ImportAssociations;
class MetaAssociations;
class StringAssociations;
class SymbolAssociations;


}

namespace Wt{ namespace Dbo{

template<>
struct dbo_traits<file_analysis::Import> : public dbo_default_traits 
{
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

template<>
struct dbo_traits<file_analysis::Meta> : public dbo_default_traits 
{
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

template<>
struct dbo_traits<file_analysis::Symbol> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::Section> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::String> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::File> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::ImportAssociations> : public dbo_default_traits {
    typedef long IDType;
        static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::MetaAssociations> : public dbo_default_traits {
    typedef long IDType;
        static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::SymbolAssociations> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::StringAssociations> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};
template<>
struct dbo_traits<file_analysis::SectionAssociations> : public dbo_default_traits {
    typedef long IDType;
    static const char *versionField() {
        return 0;
    }
    static IDType invalidID() {
        return 0;
    }
    static const char *surrogateIdField() { return 0; }
};

}}

namespace file_analysis{

const std::string SYMBOLS_TABLE = "Symbols";
const std::string IMPORTS_TABLE = "Imports";
const std::string STRINGS_TABLE = "Strings";
const std::string FILES_TABLE = "Files";
const std::string SECTIONS_TABLE = "Sections";
const std::string METAS_TABLE = "Metas";

const std::string METAS_ASSOC_TABLE = "MetaAssociations";
const std::string IMPORTS_ASSOC_TABLE = "ImportAssociations";
const std::string SYMBOLS_ASSOC_TABLE = "SymbolAssociations";
const std::string SECTIONS_ASSOC_TABLE = "SectionAssociation";
const std::string STRING_ASSOC_TABLE = "StringAssociations";


class Meta{
public:
    Meta(){}

    long HashId;
    template<class Action>
    void persist(Action& a)
    {
        
    }
};


class String{
public:
    String(): long StringHashValue(0), Location(0), 
    RelativeVirtualAddress(0), String(""){}
    
    String(RBinString *rBinString): long StringHashValue(0), Location(0), 
    RelativeVirtualAddress(0), String(""){
        
        String = rBinString->string;
        RelativeVirtualAddress = rBinString->rva;
        Offset = rBinString->offset;
        Size = rBinString->size;
        Ordinal = rBinString->ordinal;
        StringHashValue = hashSymbols(String);
    }
    

    long StringHashValue;
    long Location;
    long RelativeVirtualAddress;
    long Ordinal;
    std::string String;
    dbo::collection< dbo::ptr<StringAssociations> > StringAssociation;

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, StringHashValue, "StringHashValue");    
        dbo::id(a, String, "String");
        dbo::id(a, Location, "Location");
        dbo::id(a, RelativeVirtualAddress, "RelativeVirtualAddress");
        dbo::id(a, Ordinal, "Ordinal");
        dbo::hasMany(a, StringAssociations, dbo::ManyToOne, "StringAssociation");
        
    }
};

class File{
public:
    File():
        HashId(0), Sha256(""), Md5(""), Architecture(""),
        Bits(0), RClass(""), BClass(""), Machine(""), OS(""), 
        FileType(""), SubSystem(""), IsBigEndian(false), 
        HasVirtualAddress(false), RPath(""), EntryPoint(0), 
        Timestamp(0), Entropy(0.0), FractionalEntryPoint(0.0), 
        FileName(""), Extension(""), FuzzyHash(""){}
    
        

    long HashId;
    
    std::string Sha256;
    std::string Md5;
    std::string FuzzyHash;

    dbo::collection< dbo::ptr<StringAssociations> > StringAssociations;
    dbo::collection< dbo::ptr<SectionAssociations> > SectionAssociation;
    dbo::collection< dbo::ptr<MetaAssociations> > MetaAssociation;
    dbo::collection< dbo::ptr<StringAssociations> > StringAssociation;
    dbo::collection< dbo::ptr<SymbolAssociations> > SymbolAssociation;
    
    std::string Architecture;
    int Bits;

    std::string RClass;
    std::string BClass;
    std:string Machine;

    std::string OS;
    std::string FileType;
    std::string SubSystem;

    bool IsBigEndian;
    bool HasVirtualAddress;
    std::string RPath;
    long EntryPoint;
    long Timestamp;

    double Entropy;
    double FractionalEntryPoint;
    std::string FileName;
    std::string Extension;


    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, HashId, "HashId");

        dbo::hasMany(a, StringAssociations, dbo::ManyToOne, "StringAssociation");
        dbo::hasMany(a, SectionAssociations, dbo::ManyToOne, "SectionAssociation");
        dbo::hasMany(a, SymbolAssociations, dbo::ManyToOne, "SymbolAssociation");
        dbo::hasMany(a, ImportAssociations, dbo::ManyToOne, "ImportAssociations");
        dbo::hasMany(a, MetaAssociations, dbo::ManyToOne, "MetaAssociations");

        dbo::field(a, Md5, "Md5");
        dbo::field(a, Sha256, "Sha256");
        dbo::field(a, FuzzyHash, "FuzzyHash");

        dbo::field(a, Architecture, "Architecture");
        dbo::field(a, Bits, "Bits");

        dbo::field(a, RClass, "RClass");
        dbo::field(a, BClass, "BClass");
        dbo::field(a, Machine, "Machine");

        dbo::field(a, OS, "OS");
        dbo::field(a, FileType, "FileType");
        dbo::field(a, SubSystem, "SubSystem");

        dbo::field(a, IsBigEndian, "IsBigEndian");
        dbo::field(a, HasVirtualAddress, "HasVirtualAddress");
        dbo::field(a, RPath, "RPath");
        dbo::field(a, EntryPoint, "EntryPoint");
        dbo::field(a, Timestamp, "Timestamp");

        dbo::field(a, Entropy, "Entropy");
        dbo::field(a, FractionalEntryPoint, "FractionalEntryPoint");
        dbo::field(a, FileName, "FileName");
        dbo::field(a, Extension, "Extension");

    }

};



class Import{
public:
    Import(): 
        HashId(0), Name(""), Library(""), Hexlified(""), Type(""), Bind("")
        Hint(0), Ordinal(0), Size(0), RelativeVirtualAddress(0), Offset(0), 
        parsingError(true):{}

    Import(RBinImport *rBinImport): 
        HashId(0), Name(""), Library(""), Hexlified(""), Type(""), Bind("")
        Hint(0), Ordinal(0), Size(0), RelativeVirtualAddress(0), Offset(0), 
        parsingError(true)
    {
    
            std::string import = rBinImport->name;
            std::string delimiter = "_";
            std::size_t found = import.find(delimiter);
            

            if (found != std::string::npos){
                Name = import.substr(found+1, import.size());
                Library = import.substr(0, found);
            }else{
                Name = import;
                Library = "UNKNOWN";
            }
            Bind = rBinImport->bind;
            Type = rBinImport->type;
            
            RelativeVirtualAddress = rBinImport->rva;
            Offset = rBinImport->offset;
            Size = rBinImport->size;
            
            Ordinal = rBinImport->ordinal;
            Hint = rBinImport->hint;


            Hexlified = hexlify(this->Name);
            
            std::string printableName = getPrintableString(this->Name);
            parsingError = printableName == Name ? false : true;
            
            std::string lower_libname = this->Library;
            std::transform(lower_libname.begin(), lower_libname.end(), lower_libname.begin(), ::tolower);
            
            if(parsingError)
                this->Name = Name;
            
            this->HashId = hashSymbols(lower_libname, hexlified);

    }

    dbo::collection< dbo::ptr<ImportAssociations> > ImportAssociation;
    long HashId;

    std::string Name;
    std::string Library;
    std::string Hexlified;
    std::string Type;
    std::string Bind;
    
    long Hint;
    long Ordinal;
    long Size;
    long RelativeVirtualAddress;
    long Offset;
    

    bool parsingError;

    ~Import(){
        if(this->hash_id){
        /* left for debugging purposes */
        //    std::cout << "Destroying: " << std::hex << this->hash_id << " DllName: " << this->DllName;
        //    std::cout << " FunctionName: " << this->SymbolName << std::endl;
        }
    }

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, HashId, "HashId");
        dbo::hasMany(a, ImportAssociation, dbo::ManyToMany, "ImportAssociations");
        dbo::field(a, Name,    "Name");
        dbo::field(a, Library, "Library");   
        dbo::field(a, Hexlified, "Hexlified");
        dbo::field(a, Bind, "Bind");
        dbo::field(a, Type, "Type");
        dbo::field(a, Hint, "Hint");
        dbo::field(a, Ordinal, "Ordinal");
        dbo::field(a, Size, "Size");
        dbo::field(a, RelativeVirtualAddress, "RelativeVirtualAddress");
        dbo::field(a, Offset, "Offset");
        dbo::field(a, parsingError, "ParsingError");
        
    }
};




class Symbol{
public:
    //Import(): ImportName(""), LibraryName(""), Hexlified(""), Type(""),
    //    Memory(0), Ordinal(0), Size(0), Address(0), Offset(0){}

    Symbol(RBinImport *rBinSymbol):    
            HashId(0), Hexlified(""), Name(""), Forwarder(""),
            Bind(""), Type(""), Hint(0), Ordinal(0), Size(0),
            RelativeVirtualAddress(0), Offset(0), parsingError(false){
            
            if (rBinSymbol == NULL)
                return;
            std::string import = rBinSymbol->name;
            std::string delimiter = "_";
            std::size_t found = import.find(delimiter);
            

            if (found != std::string::npos){
                Name = import.substr(found+1, import.size());
                Library = import.substr(0, found);
            }else{
                Name = import;
                Library = "UNKNOWN";
            }
            Bind = rBinSymbol->bind;
            Type = rBinSymbol->type;
            Forwarder = rBinSymbol->forwarder;
            
            RelativeVirtualAddress = rBinSymbol->rva;
            Offset = rBinSymbol->offset;
            Size = rBinSymbol->size;
            
            Ordinal = rBinSymbol->ordinal;
            Hint = rBinSymbol->hint;


            Hexlified = hexlify(this->Name);
            
            std::string printableName = getPrintableString(this->Name);
            parsingError = printableName == Name ? false : true;
            
            std::string lower_libname = this->Library;
            std::transform(lower_libname.begin(), lower_libname.end(), lower_libname.begin(), ::tolower);
            
            if(parsingError)
                this->Name = Name;
            
            this->HashId = hashSymbols(lower_libname, hexlified);

    }

    dbo::collection< dbo::ptr<SymbolAssociations> > SymbolAssociation;
    long HashId;

    std::string Hexlified;
    std::string Name;
    std::string Forwarder;
    std::string Bind;
    std::string Type;
    
    long Hint;
    long Ordinal;
    long Size;
    long RelativeVirtualAddress;
    long Offset;
    

    bool parsingError;

    ~Import(){
        if(this->hash_id){
        /* left for debugging purposes */
        //    std::cout << "Destroying: " << std::hex << this->hash_id << " DllName: " << this->DllName;
        //    std::cout << " FunctionName: " << this->SymbolName << std::endl;
        }
    }

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, HashId, "HashId");
        dbo::hasMany(a, SymbolAssociations, dbo::ManyToMany, "SymbolAssociations");
        dbo::field(a, Name,    "Name");
        
        dbo::field(a, Hexlified, "Hexlified");
        dbo::field(a, Bind, "Bind");
        dbo::field(a, Type, "Type");
        dbo::field(a, Hint, "Hint");
        dbo::field(a, Ordinal, "Ordinal");
        dbo::field(a, Size, "Size");
        dbo::field(a, RelativeVirtualAddress, "RelativeVirtualAddress");
        dbo::field(a, Offset, "Offset");
        dbo::field(a, parsingError, "ParsingError");
        
    }
};


class ImportAssociations{
public:
    dbo::ptr<Import> Import_;
    dbo::ptr<File> File_;

    ImportAssociations():{}
    
    ImportAssociations(const Import &imp, const File &pef):
    {
        this->HashId = imp.HashId ^ pef.HashId;
    }
    
    long HashId;

    // classification info    

    template<class Action>
    void persist(Action& a)
    {

        dbo::id(a, HashId, "HashId");
        
        dbo::belongsTo(a, File_, "File");
        dbo::belongsTo(a, Import_, "Import");        
    }
};


class MetaAssociations{
public:
    dbo::ptr<Meta> Meta_;
    dbo::ptr<File> File_;

    MetaAssociations():{}
    
    MetaAssociations(const Meta &meta, const File &pef):
    {
        this->HashId = meta.HashId ^ pef.HashId;
    }
    
    long HashId;

    // classification info    

    template<class Action>
    void persist(Action& a)
    {

        dbo::id(a, HashId, "HashId");
        
        dbo::belongsTo(a, File_, "File");
        dbo::belongsTo(a, Meta_, "Meta");        
    }
};


class SectionAssociations{
public:
    dbo::ptr<Section> Section_;
    dbo::ptr<File> File_;

    ImportAssociations():{}
    
    ImportAssociations(const Symbol &sec, const File &pef):
    {
        this->HashId = sec.HashId ^ pef.HashId;
    }
    
    long HashId;

    // classification info    

    template<class Action>
    void persist(Action& a)
    {

        dbo::id(a, HashId, "HashId");
        
        dbo::belongsTo(a, File_, "File");
        dbo::belongsTo(a, Section_, "Section");        
    }
};


class SymbolAssociations{
public:
    dbo::ptr<Symbol> Symbol_;
    dbo::ptr<File> File_;
    // other information about this particular pairing

    SymbolAssociations(){}
    SymbolAssociations(const Symbol &sym, const File &pef)
    {
        this->HashId = pef.HashId ^ sym.HashId;
    }

    long HashId;

    template<class Action>
    void persist(Action& a)
    {
        
        dbo::id(a, HashId, "HashId");

        dbo::belongsTo(a, File_, "File");
        dbo::belongsTo(a, Symbol_, "Symbol");


    }
};

class StringAssociations{
public:
    dbo::ptr<String> String_;
    dbo::ptr<File> File_;
    // other information about this particular pairing

    StringAssociations(){}
    StringAssociations(const String &str, const File &pef)
    {
        this->HashId = pef.HashId ^ str.HashId;
    }

    long HashId;

    template<class Action>
    void persist(Action& a)
    {
        dbo::id(a, HashId, "HashId");

        dbo::belongsTo(a, File_, "File");
        dbo::belongsTo(a, String_, "String");
    }
};



class Section{
public:

    Section(): Name(""), FractionalEntropy(0.0), Entropy(0.0), hash_id(0),
                VirtualSize(0), RawSize(0), 
                VirtualAddr(0), Characteristics(0){}

    
    Section(RBinSection *rSection ):Name(""), FractionalEntropy(0.0), Entropy(0.0), hash_id(0),
                VirtualSize(0), RawSize(0), 
                VirtualAddr(0), Characteristics(0){
            

            Name = rBinSection->name;
            
            Offset = rBinSection->offset;
            Size = rBinSection->size;
            VirtualSize = rBinSection->vsize;
            RelativeVirtualAddress = rBinSection->rva;
            

            ut64 len = rBinSection->size,
                 vlen = rBinSection->vsize,
                 offset = rBinSection->offset;

            ut8 *buf = new ut8[len];
            ut8 *vbuf = new ut8[vlen];

            r_core_read_at(rCore, offset, buf, len);
            char outStr[4096];
            
            // hash disk size 
            ut8 *hash = r_hash_do_sha256(ctxWithReset, buf, len);
            r_hex_bin2str(hash, R_HASH_SIZE_SHA256, outStr);
            OnDiskSha256 = outStr;

            ut8 *hash = r_hash_do_md5(ctxWithReset, buf, len);
            r_hex_bin2str(hash, R_HASH_SIZE_MD5, outStr);
            OnDiskMd5 = outStr;


            // hash virtual size 
            hash = r_hash_do_sha256(ctxWithReset, vbuf, vlen);
            r_hex_bin2str(hash, R_HASH_SIZE_SHA256, outStr);
            VirtualSha256 = outStr;
            
            ut8 *hash = r_hash_do_md5(ctxWithReset, vbuf, vlen);
            r_hex_bin2str(hash, R_HASH_SIZE_MD5, outStr);
            VirtualMd5 = outStr;

            Entropy = r_hash_entropy(buf, len);
            FractionalEntropy = r_hash_entropy_fraction(buf, len);
            
            delete buf;
            delete vbuf;

            hexlified = hexlify(this->SectionName);
            std::string printableName = getPrintableString(this->SectionName);
            parsingError = printableName == SectionName ? false : true;
            
            if(parsingError)
                this->SectionName = printableName;
            
            // this may not be unique, but we are storing it for
            // analysis purposes
            hash_id = hashSymbols(hexlified);
    }




    long HashId;
    dbo::collection< dbo::ptr<SectionAssociations> > SectionAssociation;
    std::string OnDiskSha256;
    std::string OnDiskMd5;
    std::string OnDiskFuzzyHash;
    std::string VirtualSha256;
    std::string VirtualMd5;
    std::string VirtualFuzzy;
    
    std::string Name;
    double Entropy;
    double FractionalEntropy;
    long Size;
    long VirtualSize;
    long RelativeVirtualAddress
    long Offset;
    bool parsingError;

    ~Section(){
    }
    template<class Action>
    void persist(Action& a)
    {

    
        dbo::id(a, HashId, "HashId");
        dbo::field(a, Name,    "Name");
        dbo::hasMany(a, SectionAssociations, dbo::ManyToMany, "SectionAssociations");
        
        dbo::field(a, Size, "Size");
        dbo::field(a, VirtualSize, "VirtualSize");
        dbo::field(a, RelativeVirtualAddress, "RelativeVirtualAddress");
        dbo::field(a, Offset, "Offset");
        dbo::field(a, parsingError, "ParsingError");
        

        dbo::field(a, OnDiskSha256, "OnDiskSha256");
        dbo::field(a, OnDiskMd5, "OnDiskMd5");
        dbo::field(a, OnDiskFuzzyHash, "OnDiskFuzzyHash");
        dbo::field(a, VirtualSha256, "VirtualSha256");
        dbo::field(a, VirtualMd5, "VirtualMd5");
        dbo::field(a, VirtualFuzzy, "VirtualFuzzy");
        dbo::field(a, Entropy, "Entropy");
        dbo::field(a, Entropy, "FractionalEntropy")
    

    }
};






}
    


#endif
