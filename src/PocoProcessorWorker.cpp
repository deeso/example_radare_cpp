
#include <Poco/Process.h>
#include <Poco/PipeStream.h>
#include <Poco/StreamCopier.h>

#include <string>
#include <iostream>
#include <fstream>
#include <iomanip> 
#include <string>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include "File.hpp"
#include "PocoProcessor.hpp"
#include "PocoProcessorWorker.hpp"

namespace Processing{
    void PocoProcessorWorker::setTaskId(long task_id){ task_id_ = task_id; }
    long PocoProcessorWorker::getTaskId(){return task_id_;}
    void PocoProcessorWorker::start(std::string &filename){
        mFile_ = NULL;
        m_Thread = new boost::thread(&PocoProcessorWorker::processFile, this, filename); 
    }
    void PocoProcessorWorker::join(){
        m_Thread->join();
    }

    void PocoProcessorWorker::releaseResultOwnerShip(){
        ownResult_ = false;
    }


    void PocoProcessorWorker::processFile(std::string &filename)
    {
     
        std::stringstream ss_out;

        std::string cmd("/usr/bin/rabin2");
        std::vector<std::string> args;
        args.push_back(std::string("-eisSIRlv"));
        args.push_back(filename);
        
        Poco::Pipe outPipe;
        Poco::ProcessHandle ph(Poco::Process::launch(cmd, args, 0, &outPipe, 0));
        Poco::PipeInputStream istr(outPipe);
        std::stringstream proc_output_str;
        int rc = ph.wait();
        Poco::StreamCopier::copyStream(istr, proc_output_str);
        
        std::cout << "*** Process completed: " << rc << std::endl;
        std::cout << "*** Output: " << proc_output_str.str() << std::endl;


        LOG4CXX_TRACE(this->myLogger, (std::string("Worker processing: ") + filename));
        
        //mFile_ = new File(filename);
        taskCompleted = true;
        {
            std::stringstream ss_out;
            ss_out << "Thread " << task_id_ << ": done processing file";    
            LOG4CXX_TRACE(this->myLogger, ss_out.str());
        }
        LOG4CXX_TRACE(this->myLogger, std::string("Calling the handler for completed files."));
        processor_.handleFileParsingCompletion(*this);
    }
    
    File *PocoProcessorWorker::getFile(){
        return NULL; 
    }
    bool PocoProcessorWorker::isAlive(){
        if (m_Thread == NULL)
            return false;
        
        boost::posix_time::time_duration delay(0,0,0,0); 
        return m_Thread->timed_join(delay); 
    }
}

