/*
    
   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef SECTION_DEF_H
#define SECTION_DEF_H

#include <boost/filesystem.hpp>
#include <boost/container/set.hpp>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <iomanip>

#include <libr/r_core.h>

class Section{

    long HashId;
    boost::container::set< long > SectionAssociation;
    std::string OnDiskSha256;
    std::string OnDiskMd5;
    std::string OnDiskFuzzyHash;
    
    std::string VirtualSha256;
    std::string VirtualMd5;
    std::string VirtualFuzzy;
    
    std::string Hexlified;
    std::string Name;
    double Entropy;
    double FractionalEntropy;
    long RawSize;
    long VirtualSize;
    long RelativeVirtualAddress;
    long Offset;
    long Permissions;
    bool ParsingError;

public:
    ~Section(){}
    Section(): HashId(0), OnDiskSha256(""), OnDiskMd5(""), OnDiskFuzzyHash(""),
            VirtualSha256(""), VirtualMd5(""), VirtualFuzzy(""),
            Hexlified(""), Name(""), Entropy(0.0), FractionalEntropy(0.0), RawSize(0),
            VirtualSize(0), RelativeVirtualAddress(0), Offset(0), Permissions(0),
            ParsingError(false){}

    
    Section(RCore *rCore, RBinSection *rBinSection ):HashId(0), 
            OnDiskSha256(""), OnDiskMd5(""), OnDiskFuzzyHash(""),
            VirtualSha256(""), VirtualMd5(""), VirtualFuzzy(""),
            Name(""), Entropy(0.0), FractionalEntropy(0.0), RawSize(0),
                VirtualSize(0), RelativeVirtualAddress(0), Offset(0), Permissions(0),
                ParsingError(false){
        
        RHash *ctx = r_hash_new(1, 0);

        Name = rBinSection->name;
        
        Offset = rBinSection->offset;
        RawSize = rBinSection->size;
        VirtualSize = rBinSection->vsize;
        RelativeVirtualAddress = rBinSection->rva;
        Permissions = rBinSection->srwx;

        ut64 len = rBinSection->size,
             vlen = rBinSection->vsize,
             offset = rBinSection->offset;

        ut8 *buf = new ut8[len];
        ut8 *vbuf = new ut8[vlen];

        r_core_read_at(rCore, offset, buf, len);
        char outStr[4096];
        
        // hash disk size 
        ut8 *hash = r_hash_do_sha256(ctx, buf, len);
        r_hex_bin2str(hash, R_HASH_SIZE_SHA256, outStr);
        OnDiskSha256 = outStr;

        hash = r_hash_do_md5(ctx, buf, len);
        r_hex_bin2str(hash, R_HASH_SIZE_MD5, outStr);
        OnDiskMd5 = outStr;


        // hash virtual size 
        hash = r_hash_do_sha256(ctx, vbuf, vlen);
        r_hex_bin2str(hash, R_HASH_SIZE_SHA256, outStr);
        VirtualSha256 = outStr;
        
        hash = r_hash_do_md5(ctx, vbuf, vlen);
        r_hex_bin2str(hash, R_HASH_SIZE_MD5, outStr);
        VirtualMd5 = outStr;

        Entropy = r_hash_entropy(buf, len);
        FractionalEntropy = r_hash_entropy_fraction(buf, len);
        
        delete buf;
        delete vbuf;

        Hexlified = hexlify(this->Name);
        std::string printableName = getPrintableString(this->Name);
        ParsingError = printableName == Name ? false : true;
        
        if(ParsingError)
            this->Name = printableName;
        
        // this may not be unique, but we are storing it for
        // analysis purposes
        HashId = hashSymbols(Hexlified);
    }


    bool operator==(Section &section){
        return section.getSha256() == getSha256();
    }
    bool operator!=(Section &section){
        return section.getSha256() != getSha256();
    }

    std::string getSha256(){
        return getOnDiskSha256();
    }

    std::string getOnDiskSha256(){
        return OnDiskSha256;
    }
    std::string getOnDiskMd5(){ return OnDiskMd5;}
    std::string getOnDiskFuzzyHash(){ return OnDiskFuzzyHash;}
    

    std::string getVirtualSha256(){ return VirtualSha256;}
    std::string getVirtualMd5() {return VirtualMd5;}
    std::string getVirtualFuzzy() {return VirtualFuzzy;}
    
    std::string getName() {return Name;}
    double getEntropy() {return Entropy;}
    double getFractionalEntropy() {return FractionalEntropy;}
    long getSize(){ return RawSize;}
    long getVirtualSize() {return VirtualSize;}
    long getRelativeVirtualAddress() {return RelativeVirtualAddress;}
    long getOffset() {return Offset;}
    bool hadParsingError() {return ParsingError;}

    std::string summarize(){
        std::stringstream ss_out;

        std::cout << " Section Name: "<< Name << std::endl;
        std::cout << " Section Offset: 0x"<< std::hex << Offset << std::endl;
        std::cout << " Section Size: 0x"<< std::hex << RawSize << std::endl;
        std::cout << " Section Virtual Size: 0x"<< std::hex <<  VirtualSize << std::endl;
        std::cout << " Section Relative Virtual Address: 0x"<< std::hex << RelativeVirtualAddress << std::endl;
        std::cout << " Section Permissions: 0"<< std::ios::oct << Permissions << std::endl;
        std::cout << " Section SHA256 Hash (disk size): 0x"<< std::hex << OnDiskSha256 << std::endl;
        std::cout << " Section SHA256 Hash (virtual size): "<< VirtualSha256 << std::endl;
        std::cout << " Section MD5 Hash (disk size): 0x"<< std::hex << OnDiskMd5 << std::endl;
        std::cout << " Section MD5 Hash (virtual size): 0x"<< std::hex << VirtualMd5 << std::endl;
        std::cout << " Section Fuzzy Hash (disk size): 0x"<< std::hex << OnDiskFuzzyHash << std::endl;
        std::cout << " Section Fuzzy Hash (virtual size): 0x"<< std::hex << VirtualFuzzy << std::endl;
        std::cout << " Section Entropy Fractional: "<< std::ios::dec << FractionalEntropy << std::endl;
        std::cout << " Section Entropy: "<< std::ios::dec << Entropy << std::endl;

        std::string out = ss_out.str();
        return out;

    }

};
#endif
