/*
    
   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include <string>


class Meta{
    long HashId;
    std::string mType;
    std::string mName; 
    std::string mSerializedValue;

    
public:
    Meta(): mName(Name), mType(Type), 
            mSerializedValue(SerializedValue){}

    Meta(const Meta & oMeta){
        this->mName = oMeta.getName();
        this->mType = oMeta.getType();
        this->mSerializedValue = oMeta.getType();
    }
    Meta(std::string Name, 
         std::string Type,
         std::string SerializedValue): mName(Name), 
                        mType(Type), mSerializedValue(SerializedValue) {}

    std::string getName(){ return mName;}
    std::string getType(){ return mType;}
    std::string getSerializedValue() {return mSerializedValue;}
};
