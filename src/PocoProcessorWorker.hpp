#ifndef RADARE_POCOPROCESSOR_WORKER_H
#define RADARE_POCOPROCESSOR_WORKER_H

#include <Poco/Process.h>

#include <string>
#include <iostream>

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <streambuf>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp> 
#include <boost/date_time.hpp>  

#include "File.hpp"
#include "Section.hpp"
//#include "PocoProcessor.hpp"

//    Worker w(3);
//    boost::thread workerThread(&Worker::processQueue, &w, 2);
namespace Processing{
class PocoProcessor;

class PocoProcessorWorker:
    public boost::enable_shared_from_this<PocoProcessorWorker>,
    private boost::noncopyable

{

    
    File * mFile_;
    PocoProcessor &processor_;
    bool taskCompleted;
    bool ownResult_;
    long task_id_;
    boost::thread * m_Thread;
    
    //PocoProcessorWorker& PocoProcessorWorker(const PocoProcessorWorker&){}
    //PocoProcessorWorker& operator=(const PocoProcessorWorker&){}
protected:
    log4cxx::LoggerPtr myLogger;
    
public:
    PocoProcessorWorker(PocoProcessor &processor):
        processor_(processor), 
        taskCompleted(false),
        ownResult_(true),
        task_id_(0),
        m_Thread(NULL){
            this->myLogger = getLogger(std::string("PocoProcessorWorker"));
        }
    
/*    PocoProcessorWorker():
        processor_(NULL), 
        taskCompleted(false),
        ownResult_(true),
        task_id_(0){}
*/
    ~PocoProcessorWorker(){
        if(ownResult_)
            delete mFile_;
    }

    void setTaskId(long task_id);
    long getTaskId();
    void start(std::string &filename);
    void join();
    bool isAlive();

    void releaseResultOwnerShip();


    void processFile(std::string &filename);
    
    File *getFile();        
};
}
#endif
