

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> 
#include <string>
#include <map>



#include <boost/regex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/random.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/date_time.hpp>

#include "File.hpp"
#include "tspriorityqueue.hpp"
#include "PocoProcessorWorker.hpp"
#include "PocoProcessor.hpp"


namespace Processing{
    // NOT a true access control, just a guidance mechanism
    void PocoProcessor::releaseAccessToCompletedFile(long fileId){
        completed_files_mutex_.lock();
        if ( file_is_being_used_.find(fileId) != file_is_being_used_.end() ){
            file_is_being_used_[fileId] = false;
        }
        completed_files_mutex_.unlock();
    }
    void PocoProcessor::releaseAccessToCompleteFilesById(std::vector<long> * fileIds){

        std::vector<long>::iterator fileIds_iter = fileIds->begin();        
        for(;fileIds_iter != fileIds->end(); fileIds_iter++){
            long fileId = *fileIds_iter;
            completed_files_mutex_.lock();
            if ( file_is_being_used_.find(fileId) != file_is_being_used_.end() ){
                file_is_being_used_[fileId] = false;
            }
            completed_files_mutex_.unlock();
            
        }


    }

    std::vector<bool> * PocoProcessor::deleteCompletedFilesById(std::vector<long> * fileIds){
        std::vector<bool> * ptr_CompletedFilesDeleteResults = new std::vector<bool>();

        std::vector<long>::iterator fileIds_iter = fileIds->begin();        

        for(;fileIds_iter != fileIds->end(); fileIds_iter++){
            long id = *fileIds_iter;
            bool result = false;
            
            completed_files_mutex_.lock();
            result = doFileDelete(id);
            completed_files_mutex_.unlock();

            ptr_CompletedFilesDeleteResults->push_back(result);
        }
        return ptr_CompletedFilesDeleteResults;
    }

    std::vector<const File*>* PocoProcessor::getCompletedFilesById(std::vector<long> * fileIds){
        std::vector<const File* > * ptr_CompletedFiles = new std::vector<const File*>();

        std::vector<long>::iterator fileIds_iter = fileIds->begin();        

        for(;fileIds_iter != fileIds->end(); fileIds_iter++){
            long id = *fileIds_iter;
            completed_files_mutex_.lock();            
            ptr_CompletedFiles->push_back(doGetFile(id));
            completed_files_mutex_.unlock();
            
        }
        return ptr_CompletedFiles;
    }
    
    long PocoProcessor::getFirstCompletedFileId(){
        boost::container::map<long, File*>::iterator iterFileMap = completed_files_.begin();
        long fid;
        completed_files_mutex_.lock();

        for(; iterFileMap != completed_files_.end(); iterFileMap++){
            
            if (file_is_being_used_[iterFileMap->first]) continue;

            fid = iterFileMap->first;
            break;

        }
        completed_files_mutex_.unlock();

        return fid;

    }

    // Right now this will read all the completed files and leave memory management 
    // e.g. delete in the hands of the user.  probably ought to make this 
    // something a bit more sophisticated and controlled.
    // meaning, do not require the user to delete the allocations
    std::vector<long> * PocoProcessor::getCompletedFileIds(){
        std::vector<long> * ptr_CompletedFilesIds = new std::vector<long>();

        boost::container::map<long, File*>::iterator iterFileMap = completed_files_.begin();
        
        completed_files_mutex_.lock();

        for(; iterFileMap != completed_files_.end(); iterFileMap++){
            
            if (file_is_being_used_[iterFileMap->first]) continue;

            ptr_CompletedFilesIds->push_back(iterFileMap->first);

        }
        completed_files_mutex_.unlock();

        return ptr_CompletedFilesIds;

    } 
    const File * PocoProcessor::getCompletedFileById(long id){
        const File * p = NULL;
        completed_files_mutex_.lock();
        p = doGetFile(id);
        completed_files_mutex_.unlock();
        return p;

    }


    const File * PocoProcessor::doGetFile(long fileId){
        const File *p = NULL;
        
        if (completed_files_.find(fileId) == completed_files_.end())
            p = NULL;
        else if (file_is_being_used_[fileId])
            p =  NULL;
        else{
            p = (completed_files_.find(fileId))->second;
            file_is_being_used_[fileId] = true;
        }
        

        return p;

    }

    void PocoProcessor::doFileAdd(long fileId, File * f){
        

        completed_files_[fileId] = f;
        file_is_being_used_[fileId] = false;
    }

    bool PocoProcessor::doFileDelete(long fileId){
        bool result = true;
        if (completed_files_.find(fileId) == completed_files_.end())
            result = false;

        else if (file_is_being_used_[fileId])
            result = false;
        
        else {
            File* theFile = completed_files_.find(fileId)->second;
            delete theFile;

            completed_files_.erase(fileId);
            file_is_being_used_.erase(fileId);

        }
        return result;
    }

    bool PocoProcessor::deleteCompletedFileById(long id){
        bool result = false;

        completed_files_mutex_.lock();        
        result = doFileDelete(id);            
        completed_files_mutex_.unlock();

        return result;

    }
    bool PocoProcessor::doDeleteTask(long taskId){
        bool result = false;

        if (tasked_threads_.find(taskId) == tasked_threads_.end()){
            result = false;
            LOG4CXX_TRACE(this->myLogger, std::string("[-] Thread not found in map."));
        }else{
            // FIXME: deleting the caller, is this reasonable, I dont think so?????
            boost::container::map<long, PocoProcessorWorker*>::iterator iterRadareWorker = tasked_threads_.find(taskId);
            PocoProcessorWorker* rWorker = iterRadareWorker->second;
            //iterRadareWorker->second = NULL;
            delete rWorker;
            tasked_threads_.erase(iterRadareWorker->first);
            running_threads_--;
            LOG4CXX_TRACE(this->myLogger, std::string("[+] Thread found, freed, and removed from map."));
            result = true;
        }

        return result;

    }

    void PocoProcessor::cleanupDeadThreads(){
        

        //boost::interprocess::interprocess_upgradable_mutex tasked_threads_mutex_;
        std::stringstream ss_out;
        std::vector<long> deleteTasks;
        tasked_threads_mutex_.lock();
        boost::container::map<long, PocoProcessorWorker*>::iterator iterTasks = tasked_threads_.begin();    
        for(; iterTasks != tasked_threads_.end(); iterTasks++){
            if (iterTasks->second->isAlive()) continue;
            deleteTasks.push_back(iterTasks->first);
        }
        tasked_threads_mutex_.unlock();
        // release lock and then delete dead threads
        std::vector<long>::iterator iterDeleteTasks = deleteTasks.begin();

        tasked_threads_mutex_.lock();
        for(; iterDeleteTasks != deleteTasks.end(); iterDeleteTasks++){
            doDeleteTask(*iterDeleteTasks);
        }
        tasked_threads_mutex_.unlock();

        for(; iterDeleteTasks != deleteTasks.end(); iterDeleteTasks++){
            ss_out.str("");
            ss_out << "[**] Reaped prematurely terminated task: " << *iterDeleteTasks; 
            LOG4CXX_TRACE(this->myLogger, ss_out.str());
        }
        

        pending_tasks_cnd_.notify_all();
    }

    void PocoProcessor::handleFileParsingCompletion(PocoProcessorWorker &radarePocoProcessorWorker){
        

        /*
        atomic operation:
            1) add the completed file to the Files map
        */

        File *file = radarePocoProcessorWorker.getFile();
        if (file){
            radarePocoProcessorWorker.releaseResultOwnerShip();
            long fid = file->getHashId();
            completed_files_mutex_.lock();
            doFileAdd(fid, file);
            completed_files_mutex_.unlock();
        }
        long taskId = radarePocoProcessorWorker.getTaskId();
        /*
        atomic operation:
            1) remove the task from the Threads map
        */

        /* remove reference to thread in the map */
        tasked_threads_mutex_.lock();        
        doDeleteTask(taskId);
        tasked_threads_mutex_.unlock();


        pending_tasks_cnd_.notify_all();
    }

    void PocoProcessor::waitThreadToComplete(){
        boost::unique_lock<boost::mutex> lock(processing_threads_mutex_);
        if (!canStartMoreThreads()){
            LOG4CXX_TRACE(this->myLogger, std::string("[=] Waiting for a thread to complete"));
            //pending_tasks_cnd_.wait(lock);
            while(!canStartMoreThreads()){
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));
            }
        }

            
    }


    bool PocoProcessor::canStartMoreThreads(){
        return running_threads_ < max_pending_threads_;
    }

    std::string PocoProcessor::startOneWorker(){
        return processNextName();
    }

    std::vector<std::string> * PocoProcessor::startMultipleWorkers(){
        std::vector<std::string> *v = new std::vector<std::string>();
        while(canStartMoreThreads() && !filenames_queue_.isEmpty()){
            v->push_back(processNextName());
            boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
        return v;
    }

    long PocoProcessor::getCurrentProcessorThreadsCnt() {
        /* 
           atomic operation: 
            1) get outstanding thread tasks count
        */
        tasked_threads_mutex_.lock();
        long val = running_threads_;
        tasked_threads_mutex_.unlock();
        return val;
    }

    long PocoProcessor::getMaxProcessorThreadsCnt(){
        return max_pending_threads_;
    }

    long PocoProcessor::getRemainingFilenameCnt(){
        return filenames_queue_.size();
    }

    std::string PocoProcessor::processNextName(){
        
        if(filenames_queue_.isEmpty()){
            return std::string();
        }
        std::string nextName = filenames_queue_.popTop();
        PocoProcessorWorker *worker = new PocoProcessorWorker(*this);
        /* 
           atomic operation: 
            1) incr current task id, 
            2) add to the map, and 
            3) set thread task id 
        */
        tasked_threads_mutex_.lock();
        running_threads_++;
        worker->setTaskId(currentProcessNumber_);
        worker->start(nextName);
        
        tasked_threads_[currentProcessNumber_] = worker;

        currentProcessNumber_++;
        // handle integer wrap, not likely
        if (currentProcessNumber_ < 0) currentProcessNumber_ = 0;
        
        tasked_threads_mutex_.unlock();
        return nextName;
    }
    void PocoProcessor::summarizeAllFiles(){
        boost::container::map<long, File*>::iterator iterFileMap = completed_files_.begin();
        
        for(;iterFileMap != completed_files_.end(); iterFileMap++){
            std::cout << (iterFileMap->second)->summarize() << std::endl;
        }
               
    }

    void PocoProcessor::clearCompletedFiles(){
        boost::container::map<long, File*>::iterator iterFileMap = completed_files_.begin();
        
        for(;iterFileMap != completed_files_.end(); iterFileMap++){
            delete (iterFileMap->second);
        }
        completed_files_.clear();

    }

    PocoProcessor::~PocoProcessor(){
        clearCompletedFiles();

    }

}