/* radare - LGPL - Copyright 2008 nibble<.ds@gmail.com> */


#ifndef _INCLUDE_R_BIN_ELF_H_
#define _INCLUDE_R_BIN_ELF_H_

#include <r_types.h>
#include "r_elf_specs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define R_BIN_ELF_SCN_IS_EXECUTABLE(x) x & SHF_EXECINSTR
#define R_BIN_ELF_SCN_IS_READABLE(x)   x & SHF_ALLOC
#define R_BIN_ELF_SCN_IS_WRITABLE(x)   x & SHF_WRITE

#define R_BIN_ELF_SYMBOLS 0x0
#define R_BIN_ELF_IMPORTS 0x1

typedef struct r_bin_elf_section_t {
	ut64 offset;
	ut64 rva;
	ut64 size;
	ut64 align;
	ut32 flags;
	char name[ELF_STRING_LENGTH];
	int last;
} RBinElfSection;

typedef struct r_bin_elf_symbol_t {
	ut64 offset;
	ut64 size;
	int ordinal;
	char bind[ELF_STRING_LENGTH];
	char type[ELF_STRING_LENGTH];
	char name[ELF_STRING_LENGTH];
	int last;
} RBinElfSymbol;

typedef struct r_bin_elf_reloc_t {
	int sym;
	int type;
	ut64 offset;
	ut64 rva;
	int last;
	char name[ELF_STRING_LENGTH];
} RBinElfReloc;

typedef struct r_bin_elf_field_t {
	ut64 offset;
	char name[ELF_STRING_LENGTH];
	int last;
} RBinElfField;

typedef struct r_bin_elf_string_t {
	ut64 offset;
	ut64 size;
	char type;
	char string[ELF_STRING_LENGTH];
	int last;
} RBinElfString;

typedef struct r_bin_elf_lib_t {
	char name[ELF_STRING_LENGTH];
	int last;
} RBinElfLib;

struct Elf32_r_bin_elf_obj_t {
        Elf32_Ehdr ehdr;
        Elf32_Phdr* phdr;
        Elf32_Shdr* shdr;
        Elf32_Shdr *strtab_section;
        ut64 strtab_size;
        char* strtab;
        Elf32_Shdr *shstrtab_section;
        ut64 shstrtab_size;
        char* shstrtab;
        int bss;
        int size;
        ut64 baddr;
        int endian;
        const char* file;
        RBuffer *b;
};

int Elf32_r_bin_elf_has_va(struct Elf32_r_bin_elf_obj_t *bin);
ut64 Elf32_r_bin_elf_get_baddr(struct Elf32_r_bin_elf_obj_t *bin);
ut64 Elf32_r_bin_elf_get_entry_offset(struct Elf32_r_bin_elf_obj_t *bin);
ut64 Elf32_r_bin_elf_get_main_offset(struct Elf32_r_bin_elf_obj_t *bin);
ut64 Elf32_r_bin_elf_get_init_offset(struct Elf32_r_bin_elf_obj_t *bin);
ut64 Elf32_r_bin_elf_get_fini_offset(struct Elf32_r_bin_elf_obj_t *bin);
int Elf32_r_bin_elf_get_stripped(struct Elf32_r_bin_elf_obj_t *bin);
int Elf32_r_bin_elf_get_static(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_data_encoding(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_arch(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_machine_name(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_file_type(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_elf_class(struct Elf32_r_bin_elf_obj_t *bin);
int Elf32_r_bin_elf_get_bits(struct Elf32_r_bin_elf_obj_t *bin);
char* Elf32_r_bin_elf_get_osabi_name(struct Elf32_r_bin_elf_obj_t *bin);
int Elf32_r_bin_elf_is_big_endian(struct Elf32_r_bin_elf_obj_t *bin);
struct r_bin_elf_reloc_t* Elf32_r_bin_elf_get_relocs(struct Elf32_r_bin_elf_obj_t *bin);
struct r_bin_elf_lib_t* Elf32_r_bin_elf_get_libs(struct Elf32_r_bin_elf_obj_t *bin);
struct r_bin_elf_section_t* Elf32_r_bin_elf_get_sections(struct Elf32_r_bin_elf_obj_t *bin);
struct r_bin_elf_symbol_t* Elf32_r_bin_elf_get_symbols(struct Elf32_r_bin_elf_obj_t *bin, int type);
struct r_bin_elf_field_t* Elf32_r_bin_elf_get_fields(struct Elf32_r_bin_elf_obj_t *bin);
char *Elf32_r_bin_elf_get_rpath(struct Elf32_r_bin_elf_obj_t *bin);
void* Elf32_r_bin_elf_free(struct Elf32_r_bin_elf_obj_t* bin);
struct Elf32_r_bin_elf_obj_t* Elf32_r_bin_elf_new(const char* file);
struct Elf32_r_bin_elf_obj_t* Elf32_r_bin_elf_new_buf(struct r_buf_t *buf);
ut64 Elf32_r_bin_elf_resize_section(struct Elf32_r_bin_elf_obj_t *bin, const char *name, ut64 size);
int Elf32_r_bin_elf_del_rpath(struct Elf32_r_bin_elf_obj_t *bin);



struct Elf64_r_bin_elf_obj_t {
        Elf64_Ehdr ehdr;
        Elf64_Phdr* phdr;
        Elf64_Shdr* shdr;
        Elf64_Shdr *strtab_section;
        ut64 strtab_size;
        char* strtab;
        Elf64_Shdr *shstrtab_section;
        ut64 shstrtab_size;
        char* shstrtab;
        int bss;
        int size;
        ut64 baddr;
        int endian;
        const char* file;
        RBuffer *b;
};

int Elf64_r_bin_elf_has_va(struct Elf64_r_bin_elf_obj_t *bin);
ut64 Elf64_r_bin_elf_get_baddr(struct Elf64_r_bin_elf_obj_t *bin);
ut64 Elf64_r_bin_elf_get_entry_offset(struct Elf64_r_bin_elf_obj_t *bin);
ut64 Elf64_r_bin_elf_get_main_offset(struct Elf64_r_bin_elf_obj_t *bin);
ut64 Elf64_r_bin_elf_get_init_offset(struct Elf64_r_bin_elf_obj_t *bin);
ut64 Elf64_r_bin_elf_get_fini_offset(struct Elf64_r_bin_elf_obj_t *bin);
int Elf64_r_bin_elf_get_stripped(struct Elf64_r_bin_elf_obj_t *bin);
int Elf64_r_bin_elf_get_static(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_data_encoding(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_arch(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_machine_name(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_file_type(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_elf_class(struct Elf64_r_bin_elf_obj_t *bin);
int Elf64_r_bin_elf_get_bits(struct Elf64_r_bin_elf_obj_t *bin);
char* Elf64_r_bin_elf_get_osabi_name(struct Elf64_r_bin_elf_obj_t *bin);
int Elf64_r_bin_elf_is_big_endian(struct Elf64_r_bin_elf_obj_t *bin);
struct r_bin_elf_reloc_t* Elf64_r_bin_elf_get_relocs(struct Elf64_r_bin_elf_obj_t *bin);
struct r_bin_elf_lib_t* Elf64_r_bin_elf_get_libs(struct Elf64_r_bin_elf_obj_t *bin);
struct r_bin_elf_section_t* Elf64_r_bin_elf_get_sections(struct Elf64_r_bin_elf_obj_t *bin);
struct r_bin_elf_symbol_t* Elf64_r_bin_elf_get_symbols(struct Elf64_r_bin_elf_obj_t *bin, int type);
struct r_bin_elf_field_t* Elf64_r_bin_elf_get_fields(struct Elf64_r_bin_elf_obj_t *bin);
char *Elf64_r_bin_elf_get_rpath(struct Elf64_r_bin_elf_obj_t *bin);
void* Elf64_r_bin_elf_free(struct Elf64_r_bin_elf_obj_t* bin);
struct Elf64_r_bin_elf_obj_t* Elf64_r_bin_elf_new(const char* file);
struct Elf64_r_bin_elf_obj_t* Elf64_r_bin_elf_new_buf(struct r_buf_t *buf);
ut64 Elf64_r_bin_elf_resize_section(struct Elf64_r_bin_elf_obj_t *bin, const char *name, ut64 size);
int Elf64_r_bin_elf_del_rpath(struct Elf64_r_bin_elf_obj_t *bin);


#ifdef __cplusplus
}
#endif

#endif

