/* radare - LGPL - Copyright 2008 nibble<.ds@gmail.com> */

#include "r_types.h"
#include "r_pe_specs.h"

#ifndef _INCLUDE_R_BIN_PE_H_
#define _INCLUDE_R_BIN_PE_H_

#ifdef __cplusplus
extern "C" {
#endif


#define R_BIN_PE_SCN_IS_SHAREABLE(x)       x & PE_IMAGE_SCN_MEM_SHARED
#define R_BIN_PE_SCN_IS_EXECUTABLE(x)      x & PE_IMAGE_SCN_MEM_EXECUTE
#define R_BIN_PE_SCN_IS_READABLE(x)        x & PE_IMAGE_SCN_MEM_READ
#define R_BIN_PE_SCN_IS_WRITABLE(x)        x & PE_IMAGE_SCN_MEM_WRITE

struct r_bin_pe_addr_t {
	ut64 rva;
	ut64 offset;
};

struct r_bin_pe_section_t {
	ut8  name[PE_IMAGE_SIZEOF_SHORT_NAME];
	ut64 size;
	ut64 vsize;
	ut64 rva;
	ut64 offset;
	ut64 flags;
	int last;
};

struct r_bin_pe_import_t {
	ut8  name[PE_NAME_LENGTH + 1];
	ut64 rva;
	ut64 offset;
	ut64 hint;
	ut64 ordinal;
	int last;
};

struct r_bin_pe_export_t {
	ut8  name[PE_NAME_LENGTH + 1];
	ut8  forwarder[PE_NAME_LENGTH + 1];
	ut64 rva;
	ut64 offset;
	ut64 ordinal;
	int last;
};

struct r_bin_pe_string_t {
	char string[PE_STRING_LENGTH];
	ut64 rva;
	ut64 offset;
	ut64 size;
	char type;
	int last;
};

struct r_bin_pe_lib_t {
	char name[PE_STRING_LENGTH];
	int last;
};



struct Pe32_r_bin_pe_obj_t {
        Pe32_image_dos_header             *dos_header;
        Pe32_image_nt_headers                     *nt_headers;
        Pe32_image_section_header         *section_header;
        Pe32_image_export_directory       *export_directory;
        Pe32_image_import_directory       *import_directory;
        Pe32_image_delay_import_directory *delay_import_directory;
        int size;
        int     endian;
    const char* file;
        struct r_buf_t* b;
};


char* Pe32_r_bin_pe_get_arch(struct Pe32_r_bin_pe_obj_t* bin);
struct r_bin_pe_addr_t* Pe32_r_bin_pe_get_entrypoint(struct Pe32_r_bin_pe_obj_t* bin);
ut64 Pe32_r_bin_pe_get_main_offset(struct Pe32_r_bin_pe_obj_t *bin);
struct r_bin_pe_export_t* Pe32_r_bin_pe_get_exports(struct Pe32_r_bin_pe_obj_t* bin); // TODO
int Pe32_r_bin_pe_get_file_alignment(struct Pe32_r_bin_pe_obj_t* bin);
ut64 Pe32_r_bin_pe_get_image_base(struct Pe32_r_bin_pe_obj_t* bin);
struct r_bin_pe_import_t* Pe32_r_bin_pe_get_imports(struct Pe32_r_bin_pe_obj_t *bin); // TODO
struct r_bin_pe_lib_t* Pe32_r_bin_pe_get_libs(struct Pe32_r_bin_pe_obj_t *bin);
int Pe32_r_bin_pe_get_image_size(struct Pe32_r_bin_pe_obj_t* bin);
char* Pe32_r_bin_pe_get_machine(struct Pe32_r_bin_pe_obj_t* bin);
char* Pe32_r_bin_pe_get_os(struct Pe32_r_bin_pe_obj_t* bin);
char* Pe32_r_bin_pe_get_class(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_get_bits(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_get_section_alignment(struct Pe32_r_bin_pe_obj_t* bin);
struct r_bin_pe_section_t* Pe32_r_bin_pe_get_sections(struct Pe32_r_bin_pe_obj_t* bin);
char* Pe32_r_bin_pe_get_subsystem(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_dll(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_big_endian(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_stripped_relocs(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_stripped_line_nums(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_stripped_local_syms(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_stripped_debug(struct Pe32_r_bin_pe_obj_t* bin);
void* Pe32_r_bin_pe_free(struct Pe32_r_bin_pe_obj_t* bin);
struct Pe32_r_bin_pe_obj_t* Pe32_r_bin_pe_new(const char* file);
struct Pe32_r_bin_pe_obj_t* Pe32_r_bin_pe_new_buf(struct r_buf_t *buf);


int Pe32_r_bin_pe_rva_to_offset(struct Pe32_r_bin_pe_obj_t* bin, int rva);
int Pe32_r_bin_pe_init_hdr(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_init_sections(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_init_imports(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_init(struct Pe32_r_bin_pe_obj_t* bin);
int Pe32_r_bin_pe_is_pie(struct Pe32_r_bin_pe_obj_t* bin);



struct Pe64_r_bin_pe_obj_t {
        Pe64_image_dos_header             *dos_header;
        Pe64_image_nt_headers                     *nt_headers;
        Pe64_image_section_header         *section_header;
        Pe64_image_export_directory       *export_directory;
        Pe64_image_import_directory       *import_directory;
        Pe64_image_delay_import_directory *delay_import_directory;
        int size;
        int     endian;
    const char* file;
        struct r_buf_t* b;
};


char* Pe64_r_bin_pe_get_arch(struct Pe64_r_bin_pe_obj_t* bin);
struct r_bin_pe_addr_t* Pe64_r_bin_pe_get_entrypoint(struct Pe64_r_bin_pe_obj_t* bin);
ut64 Pe64_r_bin_pe_get_main_offset(struct Pe64_r_bin_pe_obj_t *bin);
struct r_bin_pe_export_t* Pe64_r_bin_pe_get_exports(struct Pe64_r_bin_pe_obj_t* bin); // TODO
int Pe64_r_bin_pe_get_file_alignment(struct Pe64_r_bin_pe_obj_t* bin);
ut64 Pe64_r_bin_pe_get_image_base(struct Pe64_r_bin_pe_obj_t* bin);
struct r_bin_pe_import_t* Pe64_r_bin_pe_get_imports(struct Pe64_r_bin_pe_obj_t *bin); // TODO
struct r_bin_pe_lib_t* Pe64_r_bin_pe_get_libs(struct Pe64_r_bin_pe_obj_t *bin);
int Pe64_r_bin_pe_get_image_size(struct Pe64_r_bin_pe_obj_t* bin);
char* Pe64_r_bin_pe_get_machine(struct Pe64_r_bin_pe_obj_t* bin);
char* Pe64_r_bin_pe_get_os(struct Pe64_r_bin_pe_obj_t* bin);
char* Pe64_r_bin_pe_get_class(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_get_bits(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_get_section_alignment(struct Pe64_r_bin_pe_obj_t* bin);
struct r_bin_pe_section_t* Pe64_r_bin_pe_get_sections(struct Pe64_r_bin_pe_obj_t* bin);
char* Pe64_r_bin_pe_get_subsystem(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_dll(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_big_endian(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_stripped_relocs(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_stripped_line_nums(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_stripped_local_syms(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_stripped_debug(struct Pe64_r_bin_pe_obj_t* bin);
void* Pe64_r_bin_pe_free(struct Pe64_r_bin_pe_obj_t* bin);
struct Pe64_r_bin_pe_obj_t* Pe64_r_bin_pe_new(const char* file);
struct Pe64_r_bin_pe_obj_t* Pe64_r_bin_pe_new_buf(struct r_buf_t *buf);

int Pe64_r_bin_pe_rva_to_offset(struct Pe64_r_bin_pe_obj_t* bin, int rva);
int Pe64_r_bin_pe_init_hdr(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_init_sections(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_init_imports(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_init(struct Pe64_r_bin_pe_obj_t* bin);
int Pe64_r_bin_pe_is_pie(struct Pe64_r_bin_pe_obj_t* bin);



#ifdef __cplusplus
}
#endif

#endif
